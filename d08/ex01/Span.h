/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 13:11:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/29 15:28:34 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D08_SPAN_H
#define D08_SPAN_H
#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <functional>

class Span {
public:
	Span(void);
	Span(unsigned int N);
	Span(Span const &src);
	~Span(void);
	Span &operator=(Span const &rhs);

	unsigned int getN(void) const;
	unsigned int getPos(void) const;
	void addNumber(int num);
	void addNumber(int from, int to);
	int shortestSpan(void);
	int longestSpan(void);
	void printElements(void) const;

private:
	unsigned int _N;
	std::vector<int> _vec;
	unsigned int _pos;
};

#endif //D08_SPAN_H
