/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Span.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 13:11:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/29 15:14:29 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Span.h"

Span::Span(void) : _N(0), _pos(0){
	return;
}

Span::Span(unsigned int N) : _N(N), _pos(0) {
	return;
}

Span::~Span(void) {
	this->_vec.clear();
	return;
}

Span::Span(Span const &src) {
	*this = src;
	return;
}

Span &Span::operator=(Span const &rhs) {
	if (this != &rhs) {
		this->_N = rhs.getN();
		this->_pos = rhs.getPos();
	}
	return *this;
}

void Span::addNumber(int num) {
	try {
		if (this->_pos + 1 > this->_N)
			throw std::out_of_range("hm");
		this->_vec.push_back(num);
		this->_pos++;
	}
	catch (std::out_of_range & e) {
		std::stringstream ss;
		ss << "You can't add more numbers, this instance can only store "<<this->_N<<" elements";
		throw std::out_of_range(ss.str());
	}
}

void Span::addNumber(int from, int to) {
	try {
		for (int i = from; i < to; i++) {
			if (this->_pos + 1 > this->_N)
				throw std::out_of_range("hm");
			this->_vec.push_back(i);
			this->_pos++;
		}
	}
	catch (std::out_of_range & e) {
		std::stringstream ss;
		ss << "You can't add more numbers, this instance can only store "<<this->_N<<" elements";
		throw std::out_of_range(ss.str());
	}
}

int Span::longestSpan() {
	try {
		if (this->_pos <= 1)
			throw "Too little elements to use span";
		int tmp = *(std::min_element(this->_vec.begin(), this->_vec.end()));
		int tmp2 = *(std::max_element(this->_vec.begin(), this->_vec.end()));
		return (tmp2 - tmp);
	}
	catch (const char * str) {
		throw str;
	}
	return (0);
}

int Span::shortestSpan(){
	try {
		if (this->_pos <= 1)
			throw "Too little elements to use span";
		int tmp = *(std::min_element(this->_vec.begin(), this->_vec.end()));
		std::nth_element(this->_vec.begin(), this->_vec.begin() + 1, this->_vec.end(), std::less<int>());
		int tmp2 = this->_vec[1];
		return (tmp2 - tmp);
	}
	catch (const char * str) {
		throw str;
	}
	return (0);
}

//getters:
unsigned int Span::getN() const {
	return this->_N;
}

unsigned int Span::getPos() const {
	return this->_pos;
}

void Span::printElements() const {
	for (unsigned int i = 0; i < this->_pos; i++){
		std::cout<<this->_vec[i]<<std::endl;
	}
}
