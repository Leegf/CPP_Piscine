/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 13:04:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/29 15:14:55 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Span.h"

int 	main(void) {
	Span sp = Span(10005);

	try {
		sp.addNumber(2);
		sp.addNumber(5);
		sp.addNumber(3);
		sp.addNumber(17);
		sp.addNumber(9);

		sp.addNumber(1000, 11000);
		sp.printElements();
		std::cout<<std::endl;
		std::cout<<sp.shortestSpan()<<std::endl;
		std::cout<<sp.longestSpan()<<std::endl;
	}
	catch (std::out_of_range & e){
		std::cout<<"Exception caught: "<<e.what()<<std::endl;
	}
	catch(const char * str) {
		std::cout<<"Exception caught: "<<str<<std::endl;
	}
	return (0);
}
