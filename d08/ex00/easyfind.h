/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   easyfind.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 12:22:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/29 12:54:36 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D08_EASYFIND_H
#define D08_EASYFIND_H

#include <iostream>
#include <algorithm>
#include <vector>

template <typename Container>
bool easyfind(const Container &container, int par) {
	typename Container::const_iterator it;
	typename Container::const_iterator ite = container.end();
	try {
		for (it = container.begin(); it != ite; ++it) {
			if (*it == par)
				return true;
		}
		throw std::invalid_argument("err");
	}
	catch (std::invalid_argument & e) {
		throw std::invalid_argument("Didn't find the argument in the container.");
		return false;
	}
}

#endif //D08_EASYFIND_H
