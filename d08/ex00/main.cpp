/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 12:24:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/29 13:03:07 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "easyfind.h"

int 	main(void){
	static const int arr[] = {16, 2, 342, -42, 42, 15, 2, 3, 4, 5, 6, 7, 100, 101};
	std::vector<int> v(arr, arr + sizeof(arr)/ sizeof(arr[0]));
	try {
		std::cout << easyfind(v, 342) << std::endl;
		std::cout << easyfind(v, -42) << std::endl;
		std::cout << easyfind(v, 0) << std::endl;
		std::cout << easyfind(v, 101) << std::endl;
		std::cout << easyfind(v, 16) << std::endl;
	}
	catch (std::invalid_argument & e){
		std::cout<<"exception caught: "<<e.what()<<std::endl;
	}
	return (0);
}