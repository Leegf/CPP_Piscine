# STL

```
#include <map>
#include <list>
#include <vector>

class IOperation;

int     main(void)
{

std::list<int>
lst1.push_back(1);
lst1.push_back(2);
lst1.push_back(3);

std::list<int>::const_iterator it;
std::list<int>::const_iterator ite = lst1.end();
for (it = lst1.begin(); it != ite; ++it)
{
    std::cout<<*it<<std::endl;
}


std::map<std::string, IOperation*> map1;
std::vector<int> v1;
std::vector<int> v2(42, 100);

map1["opA"] = new OperationA;
map1["opB"] = new OperationB;

return (0);
}
```

**algorithms**

```
void displayInt(int i)
{
    std::cout<<i<<std::endl;
}


int main(void){
    std::list<int> lst;

    lst.push_back(10);
    lst.push_back(23);
    lst_push_back(3);

    for_each(lst.begin(), lst.end(), displayInt);
    return (0);
```
