/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 15:16:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/29 17:36:07 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mutantstack.h"

int 	main(void) {
	MutantStack<int> mstack;

	mstack.push(5);
	mstack.push(18);
	mstack.push(19);
	mstack.push(20);
	mstack.printElements();
	std::cout<<std::endl;

	std::cout<<mstack.top()<<std::endl;

	std::cout<<std::endl;
	std::cout<<"popping"<<std::endl;
	mstack.pop();
	std::cout<<mstack.top()<<std::endl;

	std::cout<<std::endl;
	std::cout<<"mstack size = "<<mstack.size()<<std::endl;
	std::cout<<std::endl;
	std::cout<<"Empty?: "<<mstack.empty()<<std::endl;

	MutantStack<int>::iterator it = mstack.begin(); 	MutantStack<int>::iterator ite = mstack.end();
	++it;
	--it;
	while (it != ite) {
		std::cout << *it << std::endl;
		++it;
	}
	std::stack<int> s(mstack);
//	std::cout<<s.top()<<std::endl;
	return 0;
}
