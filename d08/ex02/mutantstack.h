/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutantstack.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 16:01:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/29 17:47:34 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D08_HMM_H
#define D08_HMM_H
#include <iostream>
#include <deque>
#include <stack>

template <typename T>
class MutantStack :  std::stack<MutantStack> {
public:
	typedef typename std::deque<T> deq_type;
	typedef typename deq_type::iterator iterator;
	typedef typename deq_type::const_iterator const_iterator;
	MutantStack(void) : _size(0) {
		return;
	};
	~MutantStack(void) {
		return;
	};
	void printElements(void) const{
		std::copy(std::begin(_deq), std::end(_deq), std::ostream_iterator<T>(std::cout, " "));
/*		for (unsigned int i = 0; i < this->_size; i++){
			std::cout<<this->_deq[i]<<std::endl;
		}*/
	}
	void push(T const & content) {
		_deq.push_back(content);
		_stack.push(content);
		this->_size++;
	};
	void pop(void) {
		_deq.pop_back();
		_stack.pop();
		this->_size--;
	}
	bool empty(void) const {
		if (this->_size == 0)
			return (true);
		return (false);
	}
	MutantStack(MutantStack const &src) {
		*this = src;
	}
	MutantStack &operator=(MutantStack const &rhs){
		if (this != &rhs) {
			this->_size = rhs.size();
		}
		return *this;
	}

	T & top(void) {
		return _stack.top();
	};
	unsigned int size(void) const {
		return this->_size;
	}

	iterator begin() {return _deq.begin();}
	const_iterator cbegin() const {return _deq.cbegin(); }
	iterator end() {return _deq.end();}
	const_iterator cend() const {return _deq.cend();}

private:
	std::stack<T> _stack;
	deq_type _deq;
	unsigned int _size;
};

#endif //D08_HMM_H
