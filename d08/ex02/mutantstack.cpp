/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutantstack.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/29 15:22:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/29 15:22:33 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mutantstack.h"

mutantstack::mutantstack(void) {
	return;
}

mutantstack::~mutantstack(void) {
	return;
}

mutantstack::mutantstack(mutantstack const &src) {
	*this = src;
	return;
}

mutantstack &mutantstack::operator=(mutantstack const &rhs) {
	if (this != &rhs)
		this->_foo = rhs.getFoo();
	return *this;
}

std::ostream &operator<<(std::ostream &o, mutantstack const &i) {
	o << i.getFoo;

	return o;
}