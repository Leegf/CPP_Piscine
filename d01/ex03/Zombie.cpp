/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 18:56:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 19:18:27 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.h"

Zombie::Zombie(void): _phrase("Hehehhehey, may i bite u?)0")
{
	std::cout<<"New zombie emerged."<<std::endl;
}

Zombie::~Zombie(void)
{
	std::cout<<"Zombie "<<this->_name<<" died."<<std::endl;
}

void	Zombie::announce(void) const {
	std::cout<<"<"<<this->_name<<" ("<<this->_type<<")>"<<" "<<this->_phrase<<std::endl;
}

void	Zombie::setType(std::string type)
{
	this->_type = type;
}

void	Zombie::setName(std::string name)
{
	this->_name = name;
}
