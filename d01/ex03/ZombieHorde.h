/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 20:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 20:20:16 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_ZOMBIEHORDE_H
#define D01_ZOMBIEHORDE_H
#include <iostream>
#include "Zombie.h"

class ZombieHorde {
public:
	ZombieHorde(int N);
	~ZombieHorde(void);
	void announce(void);
private:
	static std::string _poolOfNames[];
	Zombie* _zombies;
	int	_zombieNb;
};


#endif //D01_ZOMBIEHORDE_H
