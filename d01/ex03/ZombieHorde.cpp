/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieHorde.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 20:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 20:28:52 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieHorde.h"
#include "Zombie.h"

std::string ZombieHorde::_poolOfNames[] = { "Dawne", "Bettie", "Takisha",
"Jenise", "Hang", "Latisha", "Lon", "Donetta",
"Tanesha", "Karole", "Marla", "Nicol", "Osvaldo", "Meta", "Lesley", "Effie",
"Lauralee", "Santos", "Stanton", "Argentina", "Darlena", "Lai", "Inell",
"Jeanie", "Phillis", "Janett", "Andra", "Melynda",
"Georgie", "Pa", "Jaime", "Vernia", "Ramona",
"Lucien", "Victoria", "Breann", "Ferdinand", "Tresa",
"Stella", "Marylynn", "Yoko", "Hai", "Wayne",
"Kirstin", "Shaneka", "Manuela", "Shea", "Anika",
"Allyson", "Clifton", "Fransisca", "Hilary", "Caryn",
"Weston", "Kourtney", "Mirta", "Margherita", "Renay",
"Cesar", "Neal", "Dreama", "Maris", "Eulalia",
"Nelly", "Lawana", "Jospeh", "Lue", "Sue",
"Rhonda", "Barbara", "Maile", "Amie", "Dylan",
"Oneida", "Faustina", "Mika", "Roselle", "Melody",
"Man", "Trinidad", "Yun", "Emelda", "Petronila",
"Kacie", "Hwa", "Digna", "Moises", "Taina",
"Katie", "Harland", "Kristel", "August", "Kelly",
"Kayleen", "Conchita", "Pricilla", "Xavier", "Lise",
"Brigette", "Margery"};

ZombieHorde::ZombieHorde(int N) {
	srand(clock());
	this->_zombies = new Zombie[N];
	this->_zombieNb = N;
	for (int i = 0; i < N; i++) {
		this->_zombies[i].setName(ZombieHorde::_poolOfNames[rand() % 100]);
	}
}

ZombieHorde::~ZombieHorde(void) {
	delete [] this->_zombies;
}

void	ZombieHorde::announce(void) {
	for (int i = 0; i < this->_zombieNb; i++) {
		this->_zombies[i].announce();
	}
}
