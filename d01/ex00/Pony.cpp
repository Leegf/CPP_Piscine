/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:26:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 17:56:23 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

Pony::Pony(std::string name) : _name(name)
{
	std::cout << "Pony class with name \""<<this->_name<<"\" constructed." <<std::endl;
	return ;
}

Pony::~Pony(void)
{
	std::cout << "Pony class with name \""<<this->_name<<"\" destructed." <<std::endl;
	return ;
};

void Pony::setCuteyMark(std::string mark) {
	this->_cuteymark = mark;
}

std::string Pony::getCuteyMark(void) const {
	return (this->_cuteymark);
}

void	Pony::setColor(std::string color) {
	this->_color = color;
}

std::string		Pony::getColor() const {
	return (this->_color);
}

void	Pony::do_smth(void) const {
	std::cout<<"My color is: "<<this->getColor()<<std::endl;
	std::cout<<"My special talent is: "<<this->getCuteyMark()<<std::endl;
	std::cout<<"My name is "<<this->_name<<", igogogogogogogo"<<std::endl;
}
