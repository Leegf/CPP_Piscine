/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:25:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 18:00:25 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Pony.hpp"

void ponyOnTheStack(void)
{
	Pony pony_stack("Pony1");

	pony_stack.setCuteyMark("I love cooking!");
	pony_stack.setColor("blue");
	pony_stack.do_smth();
}

void	ponyOnTheHeap(void)
{
	Pony* pony_heap = new Pony("Pony2");
	pony_heap->setCuteyMark("I love killing!");
	pony_heap->setColor("red");
	pony_heap->do_smth();
	delete pony_heap;
}

int 	main(void)
{
	ponyOnTheStack();
	ponyOnTheHeap();
	return (0);
}
