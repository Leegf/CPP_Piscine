/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Pony.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:26:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 17:55:31 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PONY_H
#define PONY_H

#include <iostream>

class Pony {
public:
	Pony(std::string name);
	~Pony(void);
	void setCuteyMark(std::string mark);
	std::string getCuteyMark(void) const;
	void setColor(std::string mark);
	std::string getColor(void) const;
	void do_smth(void) const;

private:
	std::string _name;
	std::string _cuteymark;
	std::string _color;
};

#endif
