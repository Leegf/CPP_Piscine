/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 09:51:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 10:07:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_WEAPON_H
#define D01_WEAPON_H

#include <iostream>

class Weapon {
public:
	Weapon(std::string initial_type);
	std::string const& getType() const;
	void	setType(std::string type);
private:
	std::string _type;
};

#endif //D01_WEAPON_H
