/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 09:53:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 10:59:20 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_HUMANA_H
#define D01_HUMANA_H

#include "Weapon.h"
#include <iostream>

class HumanA {
public:
	HumanA(std::string name, Weapon& wep);
	void attack(void);
private:
	Weapon&  _weapon;
	std::string _name;
};


#endif //D01_HUMANA_H
