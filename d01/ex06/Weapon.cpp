/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Weapon.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 09:51:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 11:50:39 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Weapon.h"

Weapon::Weapon(std::string initial_type) {
	this->setType(initial_type);
}

void	Weapon::setType(std::string type) {
	this->_type = type;
}

std::string const & Weapon::getType() const {
	return this->_type;
}
