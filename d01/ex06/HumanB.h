/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 09:53:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 11:53:46 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_HUMANB_H
#define D01_HUMANB_H

#include "Weapon.h"
#include <iostream>

class HumanB {
public:
	HumanB(std::string name);
	void attack(void);
	void setWeapon(Weapon &wep);
private:
	Weapon *  _weapon;
	std::string _name;
};

#endif //D01_HUMANB_H
