/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanB.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 09:53:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 11:54:06 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanB.h"
#include "Weapon.h"

HumanB::HumanB(std::string name) : _name(name) {
	return ;
}

void HumanB::attack() {
	std::cout<<this->_name<<" smites with his "<<this->_weapon->getType()<<std::endl;
}

void	HumanB::setWeapon(Weapon& wep)
{
	this->_weapon = &wep;
}
