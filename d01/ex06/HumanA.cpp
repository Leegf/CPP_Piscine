/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   HumanA.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 09:53:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 11:45:29 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "HumanA.h"
#include "Weapon.h"

HumanA::HumanA(std::string name, Weapon &wep) : _weapon(wep), _name(name) {
	return ;
}

void HumanA::attack() {
	std::cout<<this->_name<<" smites with his "<<this->_weapon.getType()<<std::endl;
}
