/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 18:57:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 20:00:50 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_ZOMBIEEVENT_H
#define D01_ZOMBIEEVENT_H
#include <iostream>
#include "Zombie.h"

class ZombieEvent {
public:
	ZombieEvent(void);
	~ZombieEvent(void);
	void setZombieType(std::string type);
	Zombie *newZombie(std::string dame);
	Zombie *randomChump(void);

private:
	std::string _zombieType;
	static std::string _poolOfNames[];
};

#endif //D01_ZOMBIEEVENT_H
