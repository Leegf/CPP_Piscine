/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ZombieEvent.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 18:57:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 20:12:30 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ZombieEvent.h"
std::string ZombieEvent::_poolOfNames[] = { "Dawne", "Bettie", "Takisha",
		"Jenise", "Hang", "Latisha", "Lon", "Donetta",
		"Tanesha", "Karole", "Marla", "Nicol", "Osvaldo", "Meta", "Lesley", "Effie",
		"Lauralee", "Santos", "Stanton", "Argentina", "Darlena", "Lai", "Inell",
		"Jeanie", "Phillis", "Janett", "Andra", "Melynda",
		"Georgie", "Pa", "Jaime", "Vernia", "Ramona",
		"Lucien", "Victoria", "Breann", "Ferdinand", "Tresa",
		"Stella", "Marylynn", "Yoko", "Hai", "Wayne",
		"Kirstin", "Shaneka", "Manuela", "Shea", "Anika",
		"Allyson", "Clifton", "Fransisca", "Hilary", "Caryn",
		"Weston", "Kourtney", "Mirta", "Margherita", "Renay",
		"Cesar", "Neal", "Dreama", "Maris", "Eulalia",
		"Nelly", "Lawana", "Jospeh", "Lue", "Sue",
		"Rhonda", "Barbara", "Maile", "Amie", "Dylan",
		"Oneida", "Faustina", "Mika", "Roselle", "Melody",
		"Man", "Trinidad", "Yun", "Emelda", "Petronila",
		"Kacie", "Hwa", "Digna", "Moises", "Taina",
		"Katie", "Harland", "Kristel", "August", "Kelly",
		"Kayleen", "Conchita", "Pricilla", "Xavier", "Lise",
		"Brigette", "Margery"};

ZombieEvent::ZombieEvent(void) {
	return ;
}

ZombieEvent::~ZombieEvent(void) {
	return ;
}

void	ZombieEvent::setZombieType(std::string type)
{
	this->_zombieType = type;
}

Zombie* ZombieEvent::newZombie(std::string name)
{
	Zombie * hm = new Zombie();
	hm->setName(name);
	hm->setType(this->_zombieType);
	return (hm);
}

Zombie* ZombieEvent::randomChump(void)
{
	srand(clock());
	Zombie * tmp;
	tmp = this->newZombie(ZombieEvent::_poolOfNames[rand() % 100]);
	tmp->announce();
	return (tmp);
}
