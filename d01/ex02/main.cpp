/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 18:56:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 19:59:21 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Zombie.h"
#include "ZombieEvent.h"

int 	main(void)
{
	ZombieEvent cat;
	cat.setZombieType("boo");
	Zombie* jack = cat.newZombie("Jack Sosiska");
	jack->announce();
	cat.setZombieType("omg");
	Zombie* lilly = cat.newZombie("Lilly");
	lilly->announce();
	delete jack;
	delete lilly;
	Zombie* murka = cat.randomChump();
	delete murka;
	Zombie* lol = cat.randomChump();
	Zombie* lol2 = cat.randomChump();
	lol->announce();
	delete lol2;
	delete lol;
	return (0);
}
