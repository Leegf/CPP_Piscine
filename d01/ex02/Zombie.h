/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Zombie.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 18:56:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 19:18:27 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_ZOMBIE_H
#define D01_ZOMBIE_H
#include <iostream>

class Zombie {
public:
	Zombie(void);
	~Zombie(void);
	void	announce(void) const;
	void	setType(std::string type);
	void	setName(std::string name);
private:
	std::string _name;
	std::string _type;
	std::string _phrase;
};


#endif
