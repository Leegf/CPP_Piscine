/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 12:05:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 15:38:48 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <fstream>

namespace evth {
	bool check_strings(char *s1, char *s2) {
		std::string str1 = s1;
		std::string str2 = s2;

		return !(str1.length() < 1 || str2.length() < 1);
	}

	void initialCheckings(int ac, char **av) {
		if (ac < 4) {
			std::cerr << "Usage: ./sed_rep filename \"s1\" \"s2\"" << std::endl;
			exit(1);
		}
		if (!evth::check_strings(av[2], av[3])) {
			std::cerr << "Strings can't be empty" << std::endl;
			exit(1);
		}
	}

	void replace(std::ifstream &ifs, char *s0, char *s1, char *s2) {
		size_t pos;
		std::string hm = s0;
		std::string rep_str = s2;
		std::ofstream ofs(hm + ".replace");
		std::string tmp;

		if (!ofs.is_open()) {
			std::cerr << "For some reason output file can't be created"
					  << std::endl;
			exit(1);
		}
		while (std::getline(ifs, tmp)) {
			pos = tmp.find(s1);
			while (pos != std::string::npos) {
				tmp.replace(pos, strlen(s1), rep_str);
				pos = tmp.find(s1, pos + 1);
			}
			ofs << tmp<< std::endl;
		}
		ofs.close();
	}
};

int 	main(int ac, char **av)
{
	std::ifstream ifs;

	evth::initialCheckings(ac, av);
	ifs.open(av[1]);
	if (!ifs.is_open()) {
		std::cerr<<"File can't be open"<<std::endl;
		exit(1);
	}
	evth::replace(ifs, av[1], av[2], av[3]);
	ifs.close();
}
