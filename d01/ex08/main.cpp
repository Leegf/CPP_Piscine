/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 14:29:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 15:06:33 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.h"

int 	main(void)
{
	Human inst;

	inst.action("intimidatingShout", "bird");
	inst.action("meleeAttack", "Bruce lee");
	inst.action("rangedAttack", "hitman");
	inst.action("rangedAttack", "sniper");
	inst.action("meleeAttack", "Tarzan");
	inst.action("intimidatingShout", "frog");
	return (0);
}