/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 14:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 15:04:49 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.h"

void Human::meleeAttack(std::string const &target) {
	std::cout<<"Human attacks "<<target<<" with his fists."<<std::endl;
}

void Human::rangedAttack(std::string const &target) {
	std::cout<<"Human attacks "<<target<<" with his catapult."<<std::endl;
}

void Human::intimidatingShout(std::string const &target) {
	std::cout<<"Human calls "<<target<<" an idiot who can't even come up with a proper insult."<<std::endl;
}

void Human::action(std::string const &action_name, std::string const &target) {
	typedef void (Human::*met_func)(std::string const &target);
	met_func met_ptrs[3] = {&Human::meleeAttack, &Human::rangedAttack, &Human::intimidatingShout};
	std::string arr[] = {"meleeAttack", "rangedAttack", "intimidatingShout"};
	for (int i = 0; i < 3; i++) {
		if (action_name.compare(arr[i]) == 0)
			(this->*met_ptrs[i])(target);
	}

}

