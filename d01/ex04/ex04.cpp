/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ex04.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 20:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 20:36:36 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

int 	main(void)
{
	std::string str = "HI THIS IS BRAIN";
	
	std::string* ptr_to_str = &str;
	std::string& ref_to_str = str;
	
	std::cout<<*ptr_to_str<<std::endl;
	std::cout<<ref_to_str<<std::endl;
	return (0);
}
