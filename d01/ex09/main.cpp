/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 15:08:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 15:45:06 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.h"

int main(void)
{
	Logger inst("test");

	inst.log("console", "omg, i'm mad now, son.");
	inst.log("file", "you're not ready, son.");
	inst.log("file", "you're disgrace for our family, son.");
	inst.log("console", "what do you know exactly about life???");
	inst.log("kek", "what do you know exactly about life???");
	return (0);
}
