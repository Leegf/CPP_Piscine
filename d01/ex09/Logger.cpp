/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logger.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 15:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 15:50:58 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Logger.h"
#include <fstream>

Logger::Logger(std::string fileName) : _fileName(fileName) {
	if (this->_fileName.length() == 0) {
		std::cerr<<"Filename can't be empty"<<std::endl;
		exit(1);
	}
	return ;
}

void Logger::log(std::string const &dest, std::string const &message){

	typedef void (Logger::*met_func)(std::string log);
	met_func met_ptrs[2] = {&Logger::_logToConsole, &Logger::_logToFile};
	std::string arr[] = {"console", "file"};
	for (int i = 0; i < 2; i++){
		if (dest.compare(arr[i]) == 0)
			(this->*met_ptrs[i])(this->_makeLogEntry(message));
	}
}

std::string	Logger::_makeLogEntry(std::string message) {
	std::string res;
	time_t here_time;
	struct tm * timeinfo;
	char buffer[20];

	bzero(buffer, 20);
	time (&here_time);
	timeinfo = localtime(&here_time);
	strftime(buffer, 20, "[%Y%m%d_%H:%M:%S]", timeinfo);
	res = buffer;
	res+= " [" + message + "]";

	return (res);
}

void Logger::_logToConsole(std::string log) {
	std::cout<<log<<std::endl;
}

void Logger::_logToFile(std::string log) {
	std::ofstream ofs;

	ofs.open(this->_fileName, std::ofstream::app);

	if (!ofs.is_open()) {
		std::cerr << "Can't write info to that file."
				  << std::endl;
		exit(1);
	}
	ofs<<log<<std::endl;
	ofs.close();
}