/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Logger.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 15:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 15:51:31 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_LOGGER_H
#define D01_LOGGER_H

#include <iostream>

class Logger {
public:
	void log(std::string const & dest, std::string const & message);
	Logger(std::string fileName);
private:
	void _logToConsole(std::string log);
	void _logToFile(std::string log);
	std::string _makeLogEntry(std::string message);
	std::string _fileName;

};


#endif //D01_LOGGER_H
