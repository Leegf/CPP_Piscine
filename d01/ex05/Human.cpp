/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 20:40:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 10:07:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Human.h"

Brain const &	Human::getBrain(void) const
{
	return (this->brain_attr);
}
std::string	Human::identify() const {
	return (this->brain_attr.identify());
}