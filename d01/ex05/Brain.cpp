/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 20:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 21:21:15 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Brain.h"
#include <sstream>

Brain::Brain(void) : size(0), vulnerability("cpp"), color("pink") {
	return ;
}

std::string Brain::identify() const {
	std::stringstream string;
	string <<(void const *)this;
	return (string.str());
}
