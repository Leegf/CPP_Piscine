/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Human.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 20:40:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 09:47:01 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_HUMAN_H
#define D01_HUMAN_H

#include "Brain.h"

class Human {
public:
	const Brain brain_attr;
	std::string identify(void) const;
	Brain const & getBrain(void) const;
};


#endif //D01_HUMAN_H
