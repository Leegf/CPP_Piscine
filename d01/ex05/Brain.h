/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Brain.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 20:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 20:51:32 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D01_BRAIN_H
#define D01_BRAIN_H
#include <iostream>
class Brain {
public:
	int size;
	Brain(void);
	std::string vulnerability;
	std::string color;
	std::string	identify(void) const;
};


#endif //D01_BRAIN_H
