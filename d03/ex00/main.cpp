/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 17:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 20:26:58 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.h"

int		main(void)
{
	FragTrap trap("hbobom");
	FragTrap trap2 = trap;

	trap2.meleeAttack("dog");
	trap2.rangedAttack("cat");
	trap2.takeDamage(99);
	trap2.takeDamage(11);
	trap2.beRepaired(88);
	trap2.beRepaired(80);
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("omg");
	return (0);
}
