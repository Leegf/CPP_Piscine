/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 17:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 14:12:23 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.h"
#include "ScavTrap.h"
#include "ClapTrap.h"

int		main(void)
{
	FragTrap trap("hbobom"); FragTrap trap2 = trap;

	trap2.meleeAttack("dog");
	trap2.rangedAttack("cat");
	trap2.takeDamage(99);
	trap2.takeDamage(11);
	trap2.beRepaired(88);
	trap2.beRepaired(80);
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("omg");
	std::cout<<"Scavtrap"<<std::endl<<std::endl;

	ScavTrap kek("johny");
	ScavTrap boom(kek);
	boom.meleeAttack("dog");
	boom.rangedAttack("cat");
	boom.takeDamage(99);
	boom.takeDamage(7);
	boom.beRepaired(99);
	boom.beRepaired(10);
	boom.challengeNewcomer();
	boom.challengeNewcomer();
	boom.challengeNewcomer();
	return (0);
}
