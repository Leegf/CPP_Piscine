/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */ /*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 17:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 21:38:29 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.h"

std::string const FragTrap::_poolOfAttacks[] = {
		"Grenaaaade!",
		"Freezy peezy!",
		"Snoiped!",
		"Meet professor punch!",
		"Ready for the PUNCHline?!"
};

FragTrap::FragTrap(std::string name)
{
	std::cout<<"This time it'll be awesome, I promise!"<<std::endl;
	this->setHitPoints(100);
	this->setMaxHitPoints(100);
	this->setEnergyPoints(100);
	this->setMaxEnergyPoints(100);
	this->setLevel(1);
	this->setName(name);
	this->setMeleeAttackDamage(30);
	this->setRangedAttackDamage(20);
	this->setArmorDamageReduction(5);
	return;
}

FragTrap::FragTrap(void)
{
	std::cout<<"Let's get this party started![default constructor]"<<std::endl;
	this->setName("noname");
	this->setHitPoints(100);
	this->setMaxHitPoints(100);
	this->setEnergyPoints(100);
	this->setMaxEnergyPoints(100);
	this->setLevel(1);
	this->setMeleeAttackDamage(30);
	this->setRangedAttackDamage(20);
	this->setArmorDamageReduction(5);
	return;
}

FragTrap::~FragTrap() {
	std::cout<<"No fair! I wasn't ready."<<std::endl;
	return ;
}


void FragTrap::meleeAttack(std::string const &target) {
	std::cout<<"FR4G-TP <"<<this->getName()<<"> attacks <"<<target<<"> at melee, causing <"<<this->getMeleeAttackDamage()<<"> points of damage !"<<std::endl;
}

void FragTrap::rangedAttack(std::string const &target) {
	std::cout<<"FR4G-TP <"<<this->getName()<<"> attacks <"<<target<<"> at range, causing <"<<this->getRangedAttackDamage()<<"> points of damage !"<<std::endl;
}

void FragTrap::vaulthunter_dot_exe(std::string const &target) {
	srand(clock());
	if (this->getEnergyPoints() - 25 < 0) {
		std::cout<<"<"<<this->getName()<<"> "<<"is out of energy."<<std::endl;
		return ;
	}
	this->setEnergyPoints(this->getEnergyPoints() - 25);
	std::cout<<"FR4G-TP <"<<this->getName()<<"> attacks <"<<target<<"> shouting \""<<FragTrap::_poolOfAttacks[rand() % 5]<<"\""<<std::endl;
}

FragTrap &FragTrap::operator=(FragTrap const &rhs) {
	if (this != &rhs) {
		this->setName(rhs.getName());
		this->setHitPoints(rhs.getHitPoints());
		this->setMaxHitPoints(rhs.getMaxHitPoints());
		this->setEnergyPoints(rhs.getEnergyPoints());
		this->setMaxEnergyPoints(rhs.getMaxEnergyPoints());
		this->setLevel(rhs.getLevel());
		this->setMeleeAttackDamage(rhs.getMeleeAttackDamage());
		this->setRangedAttackDamage(rhs.getRangedAttackDamage());
		this->setArmorDamageReduction(rhs.getArmorDamageReduction());
	}
	return *this;
}

FragTrap::FragTrap(FragTrap const &src) {
	*this = src;
	return;
}
