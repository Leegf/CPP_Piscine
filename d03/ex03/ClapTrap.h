/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 21:10:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 21:26:59 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D02_CLAPTRAP_H
#define D02_CLAPTRAP_H
#include <iostream>
#include <ctime>
#include <cstdlib>

class ClapTrap {
public:
	ClapTrap(void);
	ClapTrap(ClapTrap const &src);
	~ClapTrap(void);
	ClapTrap &operator=(ClapTrap const &rhs);

	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);
	std::string getName(void) const;
	int getHitPoints(void) const;
	int getMaxHitPoints(void) const;
	int getEnergyPoints(void) const;
	int getMaxEnergyPoints(void) const;
	int getLevel(void) const;
	int getMeleeAttackDamage(void) const;
	int getRangedAttackDamage(void) const;
	int getArmorDamageReduction(void) const;

protected:
	void setHitPoints(int par);
	void setMaxHitPoints(int par);
	void setEnergyPoints(int par);
	void setMaxEnergyPoints(int par);
	void setLevel(int par);
	void setMeleeAttackDamage(int par);
	void setRangedAttackDamage(int par);
	void setArmorDamageReduction(int par);
	void setName(std::string name);
private:
	int _hitPoints;
	int _maxHitPoints;
	int _energyPoints;
	int _maxEnergyPoints;
	int _level;
	std::string _name;
	int _meleeAttackDamage;
	int _rangedAttackDamage;
	int _armorDamageReduction;
};

#endif //D02_CLAPTRAP_H
