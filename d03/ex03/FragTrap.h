/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 17:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 21:32:37 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D02_FRAGTRAP_H
#define D02_FRAGTRAP_H

#include <iostream>
#include "ClapTrap.h"

class FragTrap : public ClapTrap {
public:
	FragTrap(void);
	FragTrap(std::string name);
	FragTrap(FragTrap const &src);
	~FragTrap(void);
	FragTrap &operator=(FragTrap const &rhs);
	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);

	void vaulthunter_dot_exe(std::string const & target);

private:
	static const std::string _poolOfAttacks[];
};


#endif //D02_FRAGTRAP_H
