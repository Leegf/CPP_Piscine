/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 20:30:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 21:42:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.h"

std::string const ScavTrap::_poolOfChallenges[] = {
		"Make 30 push-ups!",
		"Touch your nose with your tongue.",
		"Count to ten in espanol.",
		"Tell about one of your greatest flaws.",
		"Tell me about your favourite manga."
};


ScavTrap::ScavTrap(void) {
	this->setHitPoints(100);
	this->setMaxHitPoints(100);
	this->setEnergyPoints(50);
	this->setMaxEnergyPoints(50);
	this->setLevel(1);
	this->setName("noname");
	this->setMeleeAttackDamage(20);
	this->setRangedAttackDamage(15);
	this->setArmorDamageReduction(3);
	std::cout<<"Let's set some traps for you :).[default constructor]"<<std::endl;
	return;
}

ScavTrap::ScavTrap(std::string name) {
	this->setHitPoints(100);
	this->setMaxHitPoints(100);
	this->setEnergyPoints(50);
	this->setMaxEnergyPoints(50);
	this->setLevel(1);
	this->setName(name);
	this->setMeleeAttackDamage(20);
	this->setRangedAttackDamage(15);
	this->setArmorDamageReduction(3);
	std::cout<<"Phewwwwwww, TRAPS ARE COMING, COMRADE."<<std::endl;
	return;
}

ScavTrap::~ScavTrap() {
	std::cout<<"Why... I haven't got enough fun yet."<<std::endl;
	return ;
}

void ScavTrap::meleeAttack(std::string const &target) {
	std::cout<<"SC4V-TP <"<<this->getName()<<"> attacks <"<<target<<"> at melee, causing <"<<this->getMeleeAttackDamage()<<"> points of damage !"<<std::endl;
}

void ScavTrap::rangedAttack(std::string const &target) {
	std::cout<<"SC4V-TP <"<<this->getName()<<"> attacks <"<<target<<"> at range, causing <"<<this->getRangedAttackDamage()<<"> points of damage !"<<std::endl;
}

void ScavTrap::challengeNewcomer(void) {
	srand(clock());
	if (this->getEnergyPoints() - 25 < 0) {
		std::cout<<"<"<<this->getName()<<"> "<<"is out of energy."<<std::endl;
		return ;
	}
	this->setEnergyPoints(this->getEnergyPoints() - 25);
	std::cout<<"SC4V-TP <"<<this->getName()<<"> sets a new challange for you: \""<<ScavTrap::_poolOfChallenges[rand() % 5]<<"\""<<std::endl;
}

ScavTrap &ScavTrap::operator=(ScavTrap const &rhs) {
	if (this != &rhs) {
		this->setName(rhs.getName());
		this->setHitPoints(rhs.getHitPoints());
		this->setMaxHitPoints(rhs.getMaxHitPoints());
		this->setEnergyPoints(rhs.getEnergyPoints());
		this->setMaxEnergyPoints(rhs.getMaxEnergyPoints());
		this->setLevel(rhs.getLevel());
		this->setMeleeAttackDamage(rhs.getMeleeAttackDamage());
		this->setRangedAttackDamage(rhs.getRangedAttackDamage());
		this->setArmorDamageReduction(rhs.getArmorDamageReduction());
	}
	return *this;
}

ScavTrap::ScavTrap(ScavTrap const &src) {
	*this = src;
	return;
}
