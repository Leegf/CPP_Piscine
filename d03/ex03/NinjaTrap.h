/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 10:08:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 15:39:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D02_NINJATRAP_H
#define D02_NINJATRAP_H
#include "ClapTrap.h"
#include "ScavTrap.h"
#include "FragTrap.h"

class NinjaTrap : virtual public ClapTrap {
public:
	NinjaTrap(void);
	NinjaTrap(std::string name);
	NinjaTrap(NinjaTrap const &src);
	~NinjaTrap(void);
	NinjaTrap &operator=(NinjaTrap const &rhs);

	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);
	void ninjaShoebox(const NinjaTrap&);
	void ninjaShoebox(const ScavTrap&);
	void ninjaShoebox(const FragTrap&);
	void ninjaShoebox(const ClapTrap&);

private:
};


#endif //D02_NINJATRAP_H
