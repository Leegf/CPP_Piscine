/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ScavTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 20:30:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 21:01:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ScavTrap.h"

std::string const ScavTrap::_poolOfChallenges[] = {
		"Make 30 push-ups!",
		"Touch your nose with your tongue.",
		"Count to ten in espanol.",
		"Tell about one of your greatest flaws.",
		"Tell me about your favourite manga."
};


ScavTrap::ScavTrap(void) : _hitPoints(100), _maxHitPoints(100),
_energyPoints(50), _maxEnergyPoints(50), _level(1), _name("noname"),
_meleeAttackDamage(20), _rangedAttackDamage(15), _armorDamageReduction(3) {
	std::cout<<"Let's set some traps for you :).[default constructor]"<<std::endl;
	return;
}

ScavTrap::ScavTrap(std::string name) : _hitPoints(100), _maxHitPoints(100),
_energyPoints(50), _maxEnergyPoints(50), _level(1), _name(name),
_meleeAttackDamage(20), _rangedAttackDamage(15), _armorDamageReduction(3) {
	std::cout<<"Phewwwwwww, TRAPS ARE COMING, COMRADE."<<std::endl;
	return;
}

ScavTrap &ScavTrap::operator=(ScavTrap const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoints();
		this->_maxHitPoints = rhs.getMaxHitPoints();
		this->_energyPoints = rhs.getEnergyPoints();
		this->_maxEnergyPoints = rhs.getMaxEnergyPoints();
		this->_level = rhs.getLevel();
		this->_meleeAttackDamage = rhs.getMeleeAttackDamage();
		this->_rangedAttackDamage = rhs.getRangedAttackDamage();
		this->_armorDamageReduction = rhs.getArmorDamageReduction();
	}
	return *this;
}

ScavTrap::~ScavTrap() {
	std::cout<<"Why... I haven't got enough fun yet."<<std::endl;
	return ;
}

void ScavTrap::meleeAttack(std::string const &target) {
	std::cout<<"SC4V-TP <"<<this->getName()<<"> attacks <"<<target<<"> at melee, causing <"<<this->_meleeAttackDamage<<"> points of damage !"<<std::endl;
}

void ScavTrap::rangedAttack(std::string const &target) {
	std::cout<<"SC4V-TP <"<<this->getName()<<"> attacks <"<<target<<"> at range, causing <"<<this->_rangedAttackDamage<<"> points of damage !"<<std::endl;
}

void ScavTrap::takeDamage(unsigned int amount) {
	if (((this->_hitPoints - (int)(amount - this->_armorDamageReduction)) < 0) || ((int)amount <= this->_armorDamageReduction)) {
		std::cout << "Can't take that damage." << std::endl;
		return ;
	}
	std::cout<<"SC4V-TP <"<<this->getName()<<"> takes "<<((int)amount - this->_armorDamageReduction)<<" points of damage";
	this->_hitPoints -= (int)amount - this->_armorDamageReduction;
	std::cout<<" [current HP: "<<this->getHitPoints()<<"]"<<std::endl;
}

void ScavTrap::beRepaired(unsigned int amount) {
	if ((int)(amount + this->_hitPoints) > this->_maxHitPoints) {
		std::cout << "HP is already at its maximum." << std::endl;
		return ;
	}
	std::cout<<"SC4V-TP <"<<this->getName()<<"> restores HP on "<<amount;
	this->_hitPoints += amount;
	std::cout<<" [current HP: "<<this->getHitPoints()<<"]"<<std::endl;
}
ScavTrap::ScavTrap(ScavTrap const &src) {
	*this = src;
	return;
}

void ScavTrap::challengeNewcomer(void) {
	srand(clock());
	if (this->_energyPoints - 25 < 0) {
		std::cout<<"<"<<this->getName()<<"> "<<"is out of energy."<<std::endl;
		return ;
	}
	this->_energyPoints -= 25;
	std::cout<<"SC4V-TP <"<<this->getName()<<"> sets a new challange for you: \""<<ScavTrap::_poolOfChallenges[rand() % 5]<<"\""<<std::endl;
}

//getters

std::string ScavTrap::getName() const{
	return this->_name;
}

int ScavTrap::getHitPoints() const {
	return this->_hitPoints;
}

int ScavTrap::getMaxHitPoints() const {
	return this->_maxHitPoints;
}

int ScavTrap::getEnergyPoints() const {
	return this->_energyPoints;
}

int ScavTrap::getMaxEnergyPoints() const {
	return this->_maxEnergyPoints;
}

int ScavTrap::getLevel() const {
	return this->_level;
}

int ScavTrap::getMeleeAttackDamage() const {
	return this->_meleeAttackDamage;
}

int ScavTrap::getRangedAttackDamage() const {
	return this->_rangedAttackDamage;
}

int ScavTrap::getArmorDamageReduction() const {
	return this->_armorDamageReduction;
}
