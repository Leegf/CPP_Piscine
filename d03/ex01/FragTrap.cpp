/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   FragTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 17:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 20:24:36 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.h"

std::string const FragTrap::_poolOfAttacks[] = {
		"Grenaaaade!",
		"Freezy peezy!",
		"Snoiped!",
		"Meet professor punch!",
		"Ready for the PUNCHline?!"
};

FragTrap::FragTrap(std::string name) : _hitPoints(100), _maxHitPoints(100),
_energyPoints(100), _maxEnergyPoints(100), _level(1), _name(name),
_meleeAttackDamage(30), _rangedAttackDamage(20), _armorDamageReduction(5) {
	std::cout<<"This time it'll be awesome, I promise!"<<std::endl;
	return;
}

FragTrap::FragTrap(void) : _hitPoints(100), _maxHitPoints(100),
_energyPoints(100), _maxEnergyPoints(100), _level(1), _name("noname"), _meleeAttackDamage(30),
_rangedAttackDamage(20), _armorDamageReduction(5) {
	std::cout<<"Let's get this party started![default constructor]"<<std::endl;
	return;
}

FragTrap::~FragTrap() {
	std::cout<<"No fair! I wasn't ready."<<std::endl;
	return ;
}

FragTrap::FragTrap(FragTrap const &src) {
	*this = src;
	return;
}

FragTrap &FragTrap::operator=(FragTrap const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoints();
		this->_maxHitPoints = rhs.getMaxHitPoints();
		this->_energyPoints = rhs.getEnergyPoints();
		this->_maxEnergyPoints = rhs.getMaxEnergyPoints();
		this->_level = rhs.getLevel();
		this->_meleeAttackDamage = rhs.getMeleeAttackDamage();
		this->_rangedAttackDamage = rhs.getRangedAttackDamage();
		this->_armorDamageReduction = rhs.getArmorDamageReduction();
	}
	return *this;
}


void FragTrap::meleeAttack(std::string const &target) {
	std::cout<<"FR4G-TP <"<<this->getName()<<"> attacks <"<<target<<"> at melee, causing <"<<this->_meleeAttackDamage<<"> points of damage !"<<std::endl;
}

void FragTrap::rangedAttack(std::string const &target) {
	std::cout<<"FR4G-TP <"<<this->getName()<<"> attacks <"<<target<<"> at range, causing <"<<this->_rangedAttackDamage<<"> points of damage !"<<std::endl;
}

void FragTrap::takeDamage(unsigned int amount) {
	if (((this->_hitPoints - (int)(amount - this->_armorDamageReduction)) < 0) || ((int)amount <= this->_armorDamageReduction)) {
		std::cout << "Can't take that damage." << std::endl;
		return ;
	}
	std::cout<<"FR4G-TP <"<<this->getName()<<"> takes damage at "<<((int)amount - this->_armorDamageReduction);
	this->_hitPoints -= (int)amount - this->_armorDamageReduction;
	std::cout<<" [current HP: "<<this->getHitPoints()<<"]"<<std::endl;
}

void FragTrap::beRepaired(unsigned int amount) {
	if ((int)(amount + this->_hitPoints) > this->_maxHitPoints) {
		std::cout << "HP is already at its maximum." << std::endl;
		return ;
	}
	std::cout<<"FR4G-TP <"<<this->getName()<<"> restores HP on "<<amount;
	this->_hitPoints += amount;
	std::cout<<" [current HP: "<<this->getHitPoints()<<"]"<<std::endl;
}

void FragTrap::vaulthunter_dot_exe(std::string const &target) {
	srand(clock());
	if (this->_energyPoints - 25 < 0) {
		std::cout<<"<"<<this->getName()<<"> "<<"is out of energy."<<std::endl;
		return ;
	}
	this->_energyPoints -= 25;
	std::cout<<"FR4G-TP <"<<this->getName()<<"> attacks <"<<target<<"> shouting \""<<FragTrap::_poolOfAttacks[rand() % 5]<<"\""<<std::endl;
}

//getters

std::string FragTrap::getName() const{
	return this->_name;
}

int FragTrap::getHitPoints() const {
	return this->_hitPoints;
}

int FragTrap::getMaxHitPoints() const {
	return this->_maxHitPoints;
}

int FragTrap::getEnergyPoints() const {
	return this->_energyPoints;
}

int FragTrap::getMaxEnergyPoints() const {
	return this->_maxEnergyPoints;
}

int FragTrap::getLevel() const {
	return this->_level;
}

int FragTrap::getMeleeAttackDamage() const {
	return this->_meleeAttackDamage;
}

int FragTrap::getRangedAttackDamage() const {
	return this->_rangedAttackDamage;
}

int FragTrap::getArmorDamageReduction() const {
	return this->_armorDamageReduction;
}
