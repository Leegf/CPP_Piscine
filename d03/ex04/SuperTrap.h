/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 15:06:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 15:35:17 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D02_SUPERTRAP_H
#define D02_SUPERTRAP_H

#include "FragTrap.h"
#include "NinjaTrap.h"

class SuperTrap : public FragTrap, public NinjaTrap {
public:
	SuperTrap(std::string name = "noname");
	SuperTrap(SuperTrap const &src);
	~SuperTrap(void);
	SuperTrap &operator=(SuperTrap const &rhs);

	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);

};


#endif //D02_SUPERTRAP_H
