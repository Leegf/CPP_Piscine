/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClapTrap.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 21:10:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 16:18:23 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClapTrap.h"

ClapTrap::ClapTrap(void){
std::cout<<"ClapTrap is emerging."<<std::endl;
return;
}

ClapTrap &ClapTrap::operator=(ClapTrap const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_hitPoints = rhs.getHitPoints();
		this->_maxHitPoints = rhs.getMaxHitPoints();
		this->_energyPoints = rhs.getEnergyPoints();
		this->_maxEnergyPoints = rhs.getMaxEnergyPoints();
		this->_level = rhs.getLevel();
		this->_meleeAttackDamage = rhs.getMeleeAttackDamage();
		this->_rangedAttackDamage = rhs.getRangedAttackDamage();
		this->_armorDamageReduction = rhs.getArmorDamageReduction();
	}
	return *this;
}

ClapTrap::~ClapTrap() {
	std::cout<<"Claptrap is dead."<<std::endl;
	return ;
}

void ClapTrap::takeDamage(unsigned int amount) {
	if (((this->_hitPoints - (int)(amount - this->_armorDamageReduction)) < 0) || ((int)amount <= this->_armorDamageReduction)) {
		std::cout<<"<"<<this->getName() << "> can't take that damage." << std::endl;
		return ;
	}
	std::cout<<"<"<<this->getName()<<"> takes "<<((int)amount - this->_armorDamageReduction)<<" points of damage";
	this->_hitPoints -= (int)amount - this->_armorDamageReduction;
	std::cout<<" [current HP: "<<this->getHitPoints()<<"]"<<std::endl;
}

void ClapTrap::beRepaired(unsigned int amount) {
	if ((int)(amount + this->_hitPoints) > this->_maxHitPoints) {
		std::cout<<"<"<<this->getName() << "> HP is already at its maximum." << std::endl;
		return ;
	}
	std::cout<<"<"<<this->getName()<<"> restores HP on "<<amount;
	this->_hitPoints += amount;
	std::cout<<" [current HP: "<<this->getHitPoints()<<"]"<<std::endl;
}

ClapTrap::ClapTrap(ClapTrap const &src) {
	*this = src;
	return;
}

//getters

std::string ClapTrap::getName() const{
	return this->_name;
}

int ClapTrap::getHitPoints() const {
	return this->_hitPoints;
}

int ClapTrap::getMaxHitPoints() const {
	return this->_maxHitPoints;
}

int ClapTrap::getEnergyPoints() const {
	return this->_energyPoints;
}

int ClapTrap::getMaxEnergyPoints() const {
	return this->_maxEnergyPoints;
}

int ClapTrap::getLevel() const {
	return this->_level;
}

int ClapTrap::getMeleeAttackDamage() const {
	return this->_meleeAttackDamage;
}

int ClapTrap::getRangedAttackDamage() const {
	return this->_rangedAttackDamage;
}

int ClapTrap::getArmorDamageReduction() const {
	return this->_armorDamageReduction;
}

//setters:

void ClapTrap::setHitPoints(int par) {
	this->_hitPoints = par;
}

void ClapTrap::setMaxHitPoints(int par) {
	this->_maxHitPoints = par;
}

void ClapTrap::setEnergyPoints(int par) {
	this->_energyPoints = par;
}

void ClapTrap::setMaxEnergyPoints(int par) {
	this->_maxEnergyPoints = par;
}

void ClapTrap::setLevel(int par) {
	this->_level = par;
}

void ClapTrap::setMeleeAttackDamage(int par) {
	this->_meleeAttackDamage = par;
}

void ClapTrap::setRangedAttackDamage(int par) {
	this->_rangedAttackDamage = par;
}

void ClapTrap::setArmorDamageReduction(int par) {
	this->_armorDamageReduction = par;
}

void ClapTrap::setName(std::string name) {
	this->_name = name;
}
