/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   SuperTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 15:06:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 15:42:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "SuperTrap.h"

SuperTrap::SuperTrap(std::string name) {
	FragTrap::setHitPoints(100);
	FragTrap::setMaxHitPoints(100);
	NinjaTrap::setEnergyPoints(120);
	NinjaTrap::setMaxEnergyPoints(120);
	NinjaTrap::setLevel(1);
	NinjaTrap::setName(name);
	NinjaTrap::setMeleeAttackDamage(60);
	FragTrap::setRangedAttackDamage(20);
	FragTrap::setArmorDamageReduction(5);
	std::cout<<"I'm gonna show you who's the best trap out there, bitches."<<std::endl;
	return;
}

SuperTrap::~SuperTrap() {
	std::cout<<"whoopsie-whoops, supertrap is gone."<<std::endl;
	return ;
}

SuperTrap &SuperTrap::operator=(SuperTrap const &rhs) {
	if (this != &rhs) {
		FragTrap::setName(rhs.FragTrap::getName());
		FragTrap::setHitPoints(rhs.FragTrap::getHitPoints());
		FragTrap::setMaxHitPoints(rhs.FragTrap::getMaxHitPoints());
		FragTrap::setEnergyPoints(rhs.FragTrap::getEnergyPoints());
		FragTrap::setMaxEnergyPoints(rhs.FragTrap::getMaxEnergyPoints());
		FragTrap::setLevel(rhs.FragTrap::getLevel());
		FragTrap::setMeleeAttackDamage(rhs.FragTrap::getMeleeAttackDamage());
		FragTrap::setRangedAttackDamage(rhs.FragTrap::getRangedAttackDamage());
		FragTrap::setArmorDamageReduction(rhs.FragTrap::getArmorDamageReduction());
	}
	return *this;
}

SuperTrap::SuperTrap(SuperTrap const &src) {
	*this = src;
	return;
}

void SuperTrap::rangedAttack(std::string const & target) {
	FragTrap::rangedAttack(target);
}

void SuperTrap::meleeAttack(std::string const & target) {
	NinjaTrap::meleeAttack(target);
}
