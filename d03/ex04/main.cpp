/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/21 17:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 16:19:35 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "FragTrap.h"
#include "ScavTrap.h"
#include "ClapTrap.h"
#include "NinjaTrap.h"
#include "SuperTrap.h"

int		main(void)
{

	FragTrap trap("hbobom");
	FragTrap trap2 = trap;

	trap2.meleeAttack("dog");
	trap2.rangedAttack("cat");
	trap2.takeDamage(99);
	trap2.takeDamage(11);
	trap2.takeDamage(11);
	trap2.beRepaired(88);
	trap2.beRepaired(80);
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("boooo");
	trap2.vaulthunter_dot_exe("omg");
	std::cout<<"Scavtrap"<<std::endl<<std::endl;

	ScavTrap kek("johny");
	ScavTrap boom(kek);
	boom.meleeAttack("dog");
	boom.rangedAttack("cat");
	boom.takeDamage(99);
	boom.takeDamage(7);
	boom.takeDamage(7);
	boom.beRepaired(99);
	boom.beRepaired(10);
	boom.challengeNewcomer();
	boom.challengeNewcomer();
	boom.challengeNewcomer();
	std::cout<<"NinjaTrap"<<std::endl<<std::endl;

	ClapTrap wow;
	NinjaTrap ninja("omg");
	NinjaTrap ninja2;

	ninja.meleeAttack("hit it");
	ninja.rangedAttack("hit it");
	ninja.takeDamage(50);
	ninja.takeDamage(10);
	ninja.takeDamage(33);
	ninja.beRepaired(39);
	ninja.beRepaired(100);
	ninja.ninjaShoebox(ninja2);
	ninja.ninjaShoebox(kek);
	ninja.ninjaShoebox(trap);
	ninja.ninjaShoebox(wow);


	std::cout<<"SuperTrap"<<std::endl<<std::endl;

	SuperTrap super("superman");
	SuperTrap doctor = super;

	std::cout<<"hit points: "<<doctor.getHitPoints()<<std::endl;
	std::cout<<"enegy points: "<<doctor.getEnergyPoints()<<std::endl;
	doctor.meleeAttack("pigeon");
	doctor.rangedAttack("pig");
	doctor.takeDamage(50);
	doctor.takeDamage(50);
	doctor.takeDamage(14);
	doctor.takeDamage(5);
	doctor.beRepaired(70);
	doctor.beRepaired(31);
	doctor.vaulthunter_dot_exe("cpp");
	doctor.vaulthunter_dot_exe("cpp");
	doctor.ninjaShoebox(trap);
	doctor.ninjaShoebox(ninja);
	doctor.ninjaShoebox(wow);
	doctor.ninjaShoebox(kek);
	return (0);
}