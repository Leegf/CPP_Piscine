/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   NinjaTrap.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 10:08:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 16:37:28 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "NinjaTrap.h"

NinjaTrap::NinjaTrap(void) {
	this->setHitPoints(60);
	this->setMaxHitPoints(60);
	this->setEnergyPoints(120);
	this->setMaxEnergyPoints(120);
	this->setLevel(1);
	this->setName("noname");
	this->setMeleeAttackDamage(60);
	this->setRangedAttackDamage(5);
	this->setArmorDamageReduction(0);
	std::cout<<"stealthy af[default constructor]"<<std::endl;
	return;
}

NinjaTrap::NinjaTrap(std::string name) {
	this->setHitPoints(60);
	this->setMaxHitPoints(60);
	this->setEnergyPoints(120);
	this->setMaxEnergyPoints(120);
	this->setLevel(1);
	this->setName(name);
	this->setMeleeAttackDamage(60);
	this->setRangedAttackDamage(5);
	this->setArmorDamageReduction(0);
	std::cout<<"Carton boxes are the best choice for stealth"<<std::endl;
	return;
}

NinjaTrap::~NinjaTrap() {
	std::cout<<"I'll see you in hell..."<<std::endl;
	return ;
}

NinjaTrap &NinjaTrap::operator=(NinjaTrap const &rhs) {
	if (this != &rhs) {
		this->setName(rhs.getName());
		this->setHitPoints(rhs.getHitPoints());
		this->setMaxHitPoints(rhs.getMaxHitPoints());
		this->setEnergyPoints(rhs.getEnergyPoints());
		this->setMaxEnergyPoints(rhs.getMaxEnergyPoints());
		this->setLevel(rhs.getLevel());
		this->setMeleeAttackDamage(rhs.getMeleeAttackDamage());
		this->setRangedAttackDamage(rhs.getRangedAttackDamage());
		this->setArmorDamageReduction(rhs.getArmorDamageReduction());
	}
	return *this;
}

NinjaTrap::NinjaTrap(NinjaTrap const &src) {
	*this = src;
	return;
}

void NinjaTrap::meleeAttack(std::string const &target) {
	std::cout<<"INAC <"<<this->getName()<<"> attacks <"<<target<<"> at melee, causing <"<<this->getMeleeAttackDamage()<<"> points of damage !"<<std::endl;
}

void NinjaTrap::rangedAttack(std::string const &target) {
	std::cout<<"INAC <"<<this->getName()<<"> attacks <"<<target<<"> at range, causing <"<<this->getRangedAttackDamage()<<"> points of damage !"<<std::endl;
}

void NinjaTrap::ninjaShoebox(const ClapTrap &) {
	if (this->getEnergyPoints() < 25) {
		std::cout<<"<"<<this->getName()<<"> is out of energy"<<std::endl;
		return ;
	}
	std::cout<<"INAC Oh, hello father!"<<std::endl;
	this->setEnergyPoints(this->getEnergyPoints() - 25);
}

void NinjaTrap::ninjaShoebox(const FragTrap &) {
	if (this->getEnergyPoints() < 25) {
		std::cout<<"<"<<this->getName()<<"> is out of energy"<<std::endl;
		return ;
	}
	std::cout<<"INAC Fragtrap, what you've got to show me?"<<std::endl;
	this->setEnergyPoints(this->getEnergyPoints() - 25);
}

void NinjaTrap::ninjaShoebox(const NinjaTrap &) {
	if (this->getEnergyPoints() < 25) {
		std::cout<<"<"<<this->getName()<<"> is out of energy"<<std::endl;
		return ;
	}
	std::cout<<"INAC I'm your evil twin, hahahahahahaha!"<<std::endl;
	this->setEnergyPoints(this->getEnergyPoints() - 25);
}

void NinjaTrap::ninjaShoebox(const ScavTrap &) {
	if (this->getEnergyPoints() < 25) {
		std::cout<<"<"<<this->getName()<<"> is out of energy"<<std::endl;
		return ;
	}
	std::cout<<"INAC your challenges are way too easy, brother"<<std::endl;
	this->setEnergyPoints(this->getEnergyPoints() - 25);
}
