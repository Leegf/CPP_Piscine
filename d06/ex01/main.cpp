/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/27 17:46:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/27 18:39:29 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
struct Data {std::string s1; int n; std::string s2;};

void * serialize(void) {
	srand(clock());

	Data * ptr = new Data();
	ptr->s1.resize(8);
	ptr->s2.resize(8);
	for (int i = 0; i < 8; i++)
	{
		if (rand() % 100 < 20)
			(ptr->s1)[i] = rand() % 10 + 48;
		else
			(ptr->s1)[i] = rand() % 25 + 65;
	}
	ptr->n = rand() % 1000;
	for (int i = 0; i < 8; i++)
	{
		if (rand() % 100 < 20)
			(ptr->s2)[i] = rand() % 10 + 48;
		else
			(ptr->s2)[i] = rand() % 25 + 65;
	}
	return (reinterpret_cast<void *>(ptr));
}

Data	* deserialize(void * raw) {
	return (reinterpret_cast<Data *>(raw));
}

int		main(void){
	Data * kek = deserialize(serialize());
	std::cout<<"s1 = "<<kek->s1<<std::endl;
	std::cout<<"n = "<<kek->n<<std::endl;
	std::cout<<"s2 = "<<kek->s2<<std::endl;
	return (0);
}