**explicit**
```
class A{};
class B{};

class C {

public:
	C(A const & _) {return ;}
explicit C(B const & _) {return;}
};

//
void f(C const & _) {
	return ;
}
//

main(void) {
f( A() ); //ok
f( B() ); // not ok, because it is implicite construction is prohibited in constructor.

return 0;
}
