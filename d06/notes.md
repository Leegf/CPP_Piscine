# Casts

**Reinterpretation cast**

``` 
void * b = &a; /*implicit reinterpretation. */
void * c = (void *) &a; /*explicit reinterpretation.*/
```

**type qualifier cast**
``` 
int a = 42;
const int *d = &a;
int *b = (int *)d;
```
**classes, upcast and downcast**
```
class Parent{};
class Child1 : public Parent{};
class Child2 : public Parent{};

int main(void) {
	Child1 a;

	Parent * b = &a; //implicit upcast
	Parent * c = (Parent *)&a; //explicit upcast

	Parent * d = &a; //implicit upcast, ok.
	Child1 * e = d; // won't work., downcast.
	Child2 * f = (Child2 *) d; //may cause some issues. downcast

	return (0);
}
```

**static cast**
```
main(void) {
	int a = 42;

	double b = a; //ok
	int c = b; //no
	int d = static_cast<int>(b); okay.
	return (0);

}
```

for classes:
```
class Parent {};
class Child1:public Parent{};
class Child2:public Parent{};

class Unrelated {};

int		main(void) {
	Child1 a;

	Parent * b = &a;//implicit upcast, ok.
	Child1 * c = b; //implicit downcast, no;
	Child2 * d = static_cast<Child 2 *>(b); //explicit downcast -> ok.

	Unrelated * e = static_cast<Unrelated *>(&a); //nope.
	return (0);
}
```
