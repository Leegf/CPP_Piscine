/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Convert.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/27 14:00:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/27 16:12:40 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D06_CONVERT_H
#define D06_CONVERT_H
#include <iostream>
class Convert {
public:
	Convert(void);
	Convert(char * par);
	Convert(Convert const &src);
	~Convert(void);
	Convert &operator=(Convert const &rhs);
	char * getPar(void) const;
	void setPar(char * par);
	void outputCharConv(void);
	void outputIntConv(void);
	void outputFloatConv(void);
	void outputDoubleConv(void);

	void usage();
	bool checkCharacters(char * s);
	void eatPar(void);
	void convertEvth(void);

private:
	char * _par;
	bool _checkParImpossible();
	double _doubleVal;
	char _charVal;
	float _floatVal;
	int _intVal;
	std::string _typeVal;
	int _charValCh;
	int _intValCh;
	int _floatValCh;
	int _doubleValCh;
};

std::ostream &operator<<(std::ostream &o, Convert const &i);

#endif //D06_CONVERT_H
