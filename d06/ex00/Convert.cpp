/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Convert.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/27 14:00:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/27 20:31:52 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Convert.h"
#include <sstream>
#include <cfloat>
#include <iomanip>

Convert::Convert(void) : _charValCh(0), _intValCh(1), _floatValCh(1), _doubleValCh(1) {
	return;
}

Convert::Convert(char * par) : _par(par), _charValCh(0), _intValCh(1), _floatValCh(1), _doubleValCh(1) {
	return;
}

Convert::~Convert(void) {
	return;
}

Convert::Convert(Convert const &src) {
	*this = src;
	return;
}

Convert &Convert::operator=(Convert const &rhs) {
	if (this != &rhs)
		this->_par = rhs.getPar();
	return *this;
}

std::ostream &operator<<(std::ostream &o, Convert const &i) {
	std::string tmp = i.getPar();
	o << tmp<<std::endl;

	return o;
}

void Convert::usage() {
	std::cout<<"Usage: ./convert parameter"<<std::endl;
}

bool Convert::checkCharacters(char *s) {
	char *tmp;
	int count = 0;

	tmp = s;
	while (count < static_cast<int>(strlen(tmp))) {
		if (!isprint(static_cast<int>(*tmp)))
			return (false);
		tmp++;
		count++;
	}
	return (true);
}

bool Convert::_checkParImpossible() {
	std::string tmp2;

	tmp2 = this->getPar();
	return tmp2 == "nanf" || tmp2 == "-inff" || tmp2 == "+inff" ||
		   tmp2 == "nan" || tmp2 == "+inf" || tmp2 == "-inf" ? true : false;
}

void Convert::outputCharConv() {
	char tmp;

	tmp = this->_charVal;
	std::cout<<"char: ";
	if (this->_charValCh == 3) {
		std::cout << " impossible"<<std::endl;
		return ;
	}
	if (!this->_charValCh) {
		std::cout << " impossible"<<std::endl;
		return ;
	}
	if (this->_checkParImpossible())
		std::cout<<" impossible"<<std::endl;
	else if (!isprint(tmp))
		std::cout<<"Non displayable"<<std::endl;
	else
		std::cout<<"'"<<tmp<<"'"<<std::endl;
}

void Convert::outputIntConv() {
	std::string tmp;

	tmp = this->getPar();
	std::cout<<"int: ";
	if (this->_intValCh == 3) {
		std::cout << " impossible"<<std::endl;
		return ;
	}
	if (tmp == "+inf" || tmp == "-inf" || tmp == "+inff" || tmp == "-inff")
		std::cout<<"impossible"<<std::endl;
	else if (this->_intValCh == 1)
		std::cout<<this->_intVal<<std::endl;
	else
		std::cout<<"impossible"<<std::endl;
}

void Convert::outputFloatConv() {
	std::string tmp = this->getPar();
	std::cout<<"float: ";
	if (this->_floatValCh == 3) {
		std::cout << " impossible"<<std::endl;
		return ;
	}
	if (this->_floatValCh == 2)
		std::cout<<"nanf"<<std::endl;
	else if (this->_floatValCh == 1) {
		if (tmp == "+inf" || tmp == "-inf" || tmp == "+inff" || tmp == "-inff")
			std::cout<< this->_floatVal <<"f"<<std::endl;
		else {
			std::cout <<std::setprecision(1)<<std::fixed<<this->_floatVal<<"f"<<std::endl;
		}
	}
	else
		std::cout<<"impossible"<<std::endl;
}

void Convert::outputDoubleConv() {
	std::string tmp = this->getPar();
	std::cout<<"double: ";
	if (this->_doubleValCh == 2)
		std::cout<<"nan"<<std::endl;
	else if (this->_doubleValCh == 1) {
		if (tmp == "+inf" || tmp == "-inf" || tmp == "+inff" || tmp == "-inff")
			std::cout<< this->_doubleVal <<std::endl;
		else
			std::cout << std::setprecision(1)<<std::fixed<< this->_doubleVal << std::endl;
	}
	else
		std::cout<<"impossible"<<std::endl;
}

void Convert::eatPar() {
	char *tmp;
	tmp = this->getPar();
	std::string tmp2 = tmp;

	//char
	if (tmp[0] == '\'' && tmp[2] == '\'' && strlen(tmp) == 3) {
		this->_charVal = tmp[1];
		this->_charValCh = 1;
		return ;
	}
	//int
	size_t len = (strlen(tmp));
	this->_intVal = atoi(tmp);
	std::ostringstream len_of_num;
	len_of_num << this->_intVal;

	if (len != len_of_num.str().length())
		this->_intValCh = 0;
	if (tmp2.find(".", 0) != std::string::npos)
		this->_intValCh = 0;
	if (this->_intValCh > 2 || this->_intValCh == 1)
		return ;
	//float
	if (tmp[len] != 'f')
		this->_floatValCh = 0;
	if (tmp2 == "nanf") {
		this->_floatValCh = 2;
		this->_doubleValCh = 2;
	}
	if (tmp2 == "+inff") {
		this->_floatVal = FLT_MAX;
		this->_floatValCh = 1;
	}
	else if (tmp2 == "-inff") {
		this->_floatVal = -FLT_MAX;
		this->_floatValCh = 1;
	}
	if (this->_floatValCh == 2 || this->_floatValCh == 1)
		return ;
	//double
	this->_doubleVal = atof(tmp);
		if (tmp2 == "nan") {
			this->_doubleValCh = 2;
			this->_floatValCh = 2;
		}
		else if (tmp2 == "+inf") {
			this->_doubleVal = DBL_MAX;
			this->_doubleValCh = 1;
		}
		else if (tmp2 == "-inf") {
			this->_doubleVal = -DBL_MAX;
			this->_doubleValCh = 1;
		}
}

void Convert::convertEvth() {
	if (this->_charValCh == 1) {
		this->_doubleVal = static_cast<double>(this->_charVal);
		this->_floatVal = static_cast<float>(this->_charVal);
		this->_intVal = static_cast<int>(this->_charVal);
		this->_intValCh = 1;
		this->_floatValCh = 1;
		this->_doubleValCh = 1;
	}
	else if (this->_intValCh == 1) {
		this->_doubleVal = static_cast<double>(this->_intVal);
		this->_floatVal = static_cast<float>(this->_intVal);
		if (this->_intVal > 255 || this->_intVal < 0)
			this->_charValCh = 3;
		else
			this->_charValCh = 1;
		this->_charVal = static_cast<char>(this->_intVal);
		this->_floatValCh = 1;
		this->_doubleValCh = 1;
	}
	else if (this->_floatValCh == 1) {
		this->_doubleVal = static_cast<double>(this->_floatVal);
		this->_intVal = static_cast<int>(this->_floatVal);
		this->_charVal = static_cast<char>(this->_floatVal);
		if (this->_intVal > 255 || this->_intVal < 0)
			this->_charValCh = 3;
		else
			this->_charValCh = 1;
		if (this->_floatVal > INT_MAX || this->_floatVal < INT_MIN)
			this->_intValCh = 3;
		else
			this->_intValCh = 1;
		this->_doubleValCh = 1;
	}
	else if (this->_doubleValCh == 1)
	{
		this->_floatVal = static_cast<float>(this->_doubleVal);
		this->_intVal = static_cast<int>(this->_doubleVal);
		this->_charVal = static_cast<char>(this->_doubleVal);
		if (this->_intVal > 255 || this->_intVal < 0)
			this->_charValCh = 3;
		else
			this->_charValCh = 1;
		if (this->_doubleVal > INT_MAX || this->_doubleVal < INT_MIN)
			this->_intValCh = 3;
		else
			this->_intValCh = 1;
		if (this->_doubleVal > FLT_MAX || this->_doubleVal < -FLT_MAX)
			this->_floatValCh = 3;
		else
			this->_floatValCh = 1;

	}
}

//getters:
char* Convert::getPar() const {
	return this->_par;
}

//setters:
void Convert::setPar(char *par) {
	this->_par = par;
}