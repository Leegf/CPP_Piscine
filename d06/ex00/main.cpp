/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/27 13:44:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/27 20:32:10 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Convert.h"

int 	main(int ac, char **av){
	Convert con;
	if (ac == 1 || ac > 2) {
		con.usage();
		return (1);
	}
	con.setPar(av[1]);
	if (!con.checkCharacters(con.getPar()))
	{
		std::cout << "Some characters are not displayable." << std::endl;
		return (1);
	}
	con.eatPar();
	con.convertEvth();
	con.outputCharConv();
	con.outputIntConv();
	con.outputFloatConv();
	con.outputDoubleConv();
	return (0);
}
