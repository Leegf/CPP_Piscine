/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/27 18:42:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/27 19:50:27 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

class Base { public: virtual ~Base(void) {}};
class A : public Base{};
class B: public Base{};
class C: public Base{};

Base * generate(void) {
	srand(clock());

	A * a_class = new A();
	B * b_class = new B() ;
	C * c_class = new C();
	switch (rand() % 3) {
		case 0:
			return (a_class);
			break;
		case 1:
			return (b_class);
			break;
		case 2:
			return (c_class);
			break;
		default:
			break;
	}
	return (NULL);
}

void identify_from_pointer(Base * p){
	A * a_class = dynamic_cast<A *>(p);
	B * b_class = dynamic_cast<B *>(p);
	C * c_class = dynamic_cast<C *>(p);
	if (a_class != NULL) {
		std::cout<<"A"<<std::endl;
	}
	else if (b_class != NULL) {
		std::cout<<"B"<<std::endl;
	}
	else if (c_class != NULL){
		std::cout<<"C"<<std::endl;
	}
	else
		std::cout<<"Something went wrong."<<std::endl;
}

void identify_from_reference(Base & p) {

	try {
		A & a_class = dynamic_cast<A &>(p);
		std::cout<<"A"<<std::endl;
		(void)a_class;
	}
	catch(std::bad_cast & bc) {
	}
	try {
		B & b_class = dynamic_cast<B &>(p);
		std::cout<<"B"<<std::endl;
		(void)b_class;
	}
	catch(std::bad_cast & bc) {
	}
	try {
		C & c_class = dynamic_cast<C &>(p);
		std::cout<<"C"<<std::endl;
		(void)c_class;
	}
	catch(std::bad_cast & bc) {
	}
}

int 	main(void) {
	Base * ptr = generate();
	identify_from_pointer(ptr);
	identify_from_reference(*ptr);
}
