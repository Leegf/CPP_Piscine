**dynamic cast**

```
//<include <typeinfo>
class Parent {public: virtual ~Parent(void) {} };
class Child1: public Parent {};
class Child2: pulbic Parent {};

int main(void)
{

Child1 a;
Parent * b = &a;

Child1 * c = dynamic_cast<Child 1 *>(b);
if (c == NULL) {
std::cout <<"Conversion is NOT Ok" << std::endl;
}

try {
Child2 & d = dynamic_cast<Child2 &>(*b);
std::cout<<"It went okay"<<std::endl;
}

catch (std::bad_cast &bc ) {
std::cout <<"Conversion is NOT Ok: "<<bc.what()<<std::endl;
return (0);
}

```
**reinterpet cast**
```
int main(void) {
float a = 420.042f;

void * b = &a;
int * c = reinterpret_cast<int *>(b);
int & d = reinterpret_cast<int &>(b);

return (0);
}
```
**const cast**
```
int main(void) {

int a = 42; 
int const b  = &a; //ok
int * c = b; //nope.
int * d = const_cast<int *>(b); //ok.

return (0);
}
```
**cast operators**
```
class Foo {

public:
Foo(float const v) : _v(v) { }
float getV(void) {returh this->_v;}

operator float() {return this->_v;}
operator int() {return static_cast<int>(this->_v); }

private:
float _v;
};

main(void)
{
Foo a(420.024f);
float b = a;
int c = a;
return (0);
};
	```
