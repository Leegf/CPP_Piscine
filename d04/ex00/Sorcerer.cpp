/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 19:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 20:33:31 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.h"
#include "Peon.h"

Sorcerer::Sorcerer(std::string name, std::string title) : _name(name), _title(title) {
	std::cout<<this->getName()<<", "<<this->getTitle()<<", is born !"<<std::endl;
	return;
}

Sorcerer::~Sorcerer() {
	std::cout<<this->getName()<<", "<<this->getTitle()<<", is dead. Consequences will never be the same !"<<std::endl;
	return ;
}

Sorcerer::Sorcerer(Sorcerer const &src) {
	*this = src;
	return;
}

Sorcerer &Sorcerer::operator=(Sorcerer const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_title = rhs.getTitle();
	}
	return *this;
}

void Sorcerer::polymorph(Victim const & vict) {
	vict.getPolymorphed();
}

//getters

std::string Sorcerer::getName(void) const {
	return this->_name;
}

std::string Sorcerer::getTitle() const {
	return this->_title;
}

//operators
std::ostream & operator<<(std::ostream & o, Sorcerer const & i) {
	o<<"I am "<<i.getName()<<", "<<i.getTitle()<<", and I like ponies !"<<std::endl;

	return o;
}