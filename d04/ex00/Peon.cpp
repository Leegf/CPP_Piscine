/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 19:28:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 20:40:51 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.h"

Peon::Peon(std::string name) : Victim(name) {
	std::cout<<"Zog zog."<<std::endl;
	return;
}

Peon::~Peon() {
	std::cout<<"Bleuark..."<<std::endl;
	return ;
}

Peon::Peon(Peon const &src) {
	*this = src;
	return;
}

Peon &Peon::operator=(Peon const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
	}
	return *this;
}

void Peon::getPolymorphed() const {
	std::cout<<this->getName()<<" has been turned into a pink pony !"<<std::endl;
	return ;
}

//getters

//operators
std::ostream & operator<<(std::ostream & o, Peon const & i) {
	o<<"I am "<<i.getName()<<" and I like otters !"<<std::endl;

	return o;
}
