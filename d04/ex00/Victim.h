/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 19:28:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 20:40:51 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_VICTIM_H
#define D04_VICTIM_H
#include <iostream>

class Victim {
public:
	Victim(std::string name = "Bilbo" );
	Victim(Victim const &src);
	~Victim(void);
	Victim &operator=(Victim const &rhs);
	std::string getName(void) const;
	virtual void getPolymorphed() const;

private:
	std::string _name;
};

std::ostream & operator<<(std::ostream & o, Victim const & i);

#endif //D04_VICTIM_H
