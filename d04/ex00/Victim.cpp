/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 19:28:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 20:09:41 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.h"

Victim::Victim(std::string name) : _name(name) {
	std::cout<<"Some random victim called "<<this->getName()<<" just popped !"<<std::endl;
	return;
}

Victim::~Victim() {
	std::cout<<"Victim "<<this->getName()<<" just died for no apparent reason !"<<std::endl;
	return ;
}

Victim::Victim(Victim const &src) {
	*this = src;
	return;
}

Victim &Victim::operator=(Victim const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
	}
	return *this;
}

void Victim::getPolymorphed() const {
	std::cout<<this->getName()<<" has been turned into a cute little sheep !"<<std::endl;
	return ;
}

//getters

std::string Victim::getName(void) const {
	return this->_name;
}

//operators
std::ostream & operator<<(std::ostream & o, Victim const & i) {
	o<<"I am "<<i.getName()<<" and I like otters !"<<std::endl;

	return o;
}
