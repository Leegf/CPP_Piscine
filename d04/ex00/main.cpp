/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 19:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 20:10:01 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.h"
#include "Victim.h"
#include "Peon.h"

int 	main(void)
{
	Sorcerer robert("Robert", "the Magnificent");
	Victim jim("Jimmy");
	Peon joe("Joe");
	std::cout << robert << jim << joe;
	robert.polymorph(jim);
	robert.polymorph(joe);
	return 0;
}
