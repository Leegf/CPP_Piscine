/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 19:28:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 20:40:51 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_PEON_H
#define D04_PEON_H
#include <iostream>
#include "Victim.h"

class Peon : public Victim {
public:
	Peon(std::string name = "Meh" );
	Peon(Peon const &src);
	~Peon(void);
	Peon &operator=(Peon const &rhs);
	void getPolymorphed() const;

private:
	std::string _name;
};

std::ostream & operator<<(std::ostream & o, Peon const & i);
#endif //D04_PEON_H
