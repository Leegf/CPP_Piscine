/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/22 19:27:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/22 20:22:40 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_SORCERER_H
#define D04_SORCERER_H

#include <iostream>
#include "Victim.h"
#include "Peon.h"

class Sorcerer {
public:
	Sorcerer(std::string name = "Gendalf", std::string title = "Grey magician");
	Sorcerer(Sorcerer const &src);
	~Sorcerer(void);
	Sorcerer &operator=(Sorcerer const &rhs);
	std::string getName(void) const;
	std::string getTitle(void) const;
	void polymorph(Victim const &);

private:
	std::string _name;
	std::string _title;

};

std::ostream & operator<<(std::ostream & o, Sorcerer const & i);

#endif //D04_SORCERER_H
