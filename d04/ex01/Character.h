/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 11:50:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 12:04:49 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_CHARACTER_H
#define D04_CHARACTER_H

#include <iostream>
#include "AWeapon.h"
#include "Enemy.h"

class Character {
public:
	Character(std::string const & name);
	~Character(void);
	Character(Character const &src);
	Character &operator=(Character const &rhs);

	void recoverAP();
	void equip(AWeapon * weap);
	void attack(Enemy * en);

	std::string getName() const;
	AWeapon * getWeapon() const;
	int getAP() const;

private:
	Character(void);
	std::string _name;
	AWeapon * _weaponPtr;
	int _actionPoints;
};

std::ostream &operator<<(std::ostream &o, Character const &i);

#endif //D04_CHARACTER_H
