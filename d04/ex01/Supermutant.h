/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Supermutant.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 11:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 11:34:26 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_SUPERMUTANT_H
#define D04_SUPERMUTANT_H

#include "Enemy.h"

class Supermutant : public Enemy {
public:
	Supermutant(void);
	~Supermutant();
	void takeDamage(int dmg);

private:
};

#endif //D04_SUPERMUTANT_H
