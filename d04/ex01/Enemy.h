/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 10:57:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 11:45:55 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_ENEMY_H
#define D04_ENEMY_H
#include <iostream>

class Enemy {
public:
	Enemy(int hp, std::string const & type);
	virtual ~Enemy(void);

	std::string getType(void) const;
	int getHP() const;
	void setHP(int hp);
	virtual void takeDamage(int dmg);

	Enemy(Enemy const &src);
	Enemy &operator=(Enemy const &rhs);

private:
	Enemy(void);
	std::string _type;
	int _hp;
};

#endif //D04_ENEMY_H
