/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 11:50:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 14:43:10 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.h"

Character::Character(void) {
	return;
}

Character::Character(std::string const &name) : _name(name),_weaponPtr(NULL), _actionPoints(40) { }

Character::~Character(void) {
	return;
}

Character::Character(Character const &src) {
	*this = src;
	return;
}

Character &Character::operator=(Character const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_weaponPtr = rhs.getWeapon();
		this->_actionPoints = rhs.getAP();
	}
	return *this;
}

void Character::equip(AWeapon *weap) {
	this->_weaponPtr = weap;
}

void Character::attack(Enemy *en) {
	if (en->getHP() <= 0)
		return;
	if (this->getWeapon() == nullptr || en == nullptr)
		return ;
	if (this->_actionPoints < this->getWeapon()->getAPCost())
		return ;
	this->_actionPoints -= this->getWeapon()->getAPCost();
	std::cout<<this->getName()<<" attacks "
 <<en->getType()<<" with a "<<getWeapon()->getName()<<std::endl;
	this->getWeapon()->attack();
	en->setHP(en->getHP() - this->getWeapon()->getDamage());
	if (en->getHP() <= 0) {
		delete en;
	}
}

void Character::recoverAP() {
	if (this->getAP() + 10 > 40) {
		this->_actionPoints = 40;
		return;
	}
	this->_actionPoints += 10;
}

//getters:
std::string Character::getName() const {
	return this->_name;
}

int Character::getAP() const {
	return this->_actionPoints;
}

AWeapon* Character::getWeapon() const {
	return this->_weaponPtr;
}

std::ostream &operator<<(std::ostream &o, Character const &i) {
	if (i.getWeapon())
		o << i.getName()<<" has "<<i.getAP()<<" AP and wields a "
		  <<i.getWeapon()->getName()<<std::endl;
	else
		o << i.getName()<<" has "<<i.getAP()<<" AP and is unarmed"<<std::endl;
	return o;
}
