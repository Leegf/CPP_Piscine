/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 10:30:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 10:56:37 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PlasmaRifle.h"

PlasmaRifle::PlasmaRifle() : AWeapon("Plasma Rifle", 5, 21) { }

void PlasmaRifle::attack() const {
	std::cout<<"* piouuu piouuu piouuu *"<<std::endl;
}
