/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Supermutant.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 11:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 11:34:32 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Supermutant.h"

Supermutant::Supermutant() : Enemy(170, "Super Mutant") {
	std::cout<<"Gaaah. My want smash heads !"<<std::endl;
}


Supermutant::~Supermutant() {
	std::cout<<"Aaargh ..."<<std::endl;
}


void Supermutant::takeDamage(int dmg) {
	if (dmg <= 3)
		return ;
	if (this->getHP() - (dmg - 3) < 0)
		return;
	this->setHP(this->getHP() - dmg);
}
