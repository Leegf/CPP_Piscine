/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 10:30:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 10:56:37 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PowerFist.h"

PowerFist::PowerFist() : AWeapon("Power Fist", 8, 50) { }

void PowerFist::attack() const {
	std::cout<<"* pschhh... SBAM! *"<<std::endl;
}
