/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Enemy.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 10:57:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 11:40:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Enemy.h"

Enemy::Enemy(void) { }

Enemy::Enemy(int hp, std::string const &type) : _type(type), _hp(hp) { }

Enemy::~Enemy(void) { }

Enemy::Enemy(Enemy const &src) {
	*this = src;
}

Enemy &Enemy::operator=(Enemy const &rhs) {
	if (this != &rhs) {
		this->_type = rhs.getType();
		this->_hp = rhs.getHP();
	}
	return *this;
}

void Enemy::takeDamage(int dmg) {
	if (dmg <= 0)
		return ;
	if (this->getHP() - dmg < 0)
		return ;
	this->_hp -= dmg;
}

//getters:

int Enemy::getHP() const {
	return this->_hp;
}

std::string Enemy::getType() const {
	return this->_type;
}

//setters:

void Enemy::setHP(int hp) {
	this->_hp = hp;
}
