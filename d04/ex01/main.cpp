/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 10:25:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 14:44:57 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PlasmaRifle.h"
#include "PowerFist.h"
#include "RadScorpion.h"
#include "Supermutant.h"
#include "Character.h"

int 	main(void){

	Character* zaz = new Character("zaz");
	std::cout << *zaz;
	Enemy* b = new RadScorpion();
	AWeapon* pr = new PlasmaRifle();
	AWeapon* pf = new PowerFist();
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->equip(pf);
	zaz->attack(b);
	std::cout << *zaz;
	zaz->equip(pr);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;
	zaz->attack(b);
	std::cout << *zaz;
	zaz->attack(b);
	zaz->attack(b);
	zaz->attack(b);

	Enemy * hm = new Supermutant();
	zaz->attack(hm);
	std::cout << *zaz;
	zaz->attack(hm);
	std::cout << *zaz;
	zaz->attack(hm);
	std::cout << *zaz;
	std::cout<<hm->getHP()<<std::endl;
	zaz->attack(hm);
	std::cout << *zaz;
	std::cout<<hm->getHP()<<std::endl;
	zaz->attack(hm);
	std::cout << *zaz;
	std::cout<<hm->getHP()<<std::endl;
	zaz->recoverAP();
	zaz->attack(hm);
	std::cout << *zaz;
	std::cout<<hm->getHP()<<std::endl;
	zaz->recoverAP();
	zaz->recoverAP();
	zaz->equip(pf);
	std::cout << *zaz;
	zaz->attack(hm);
	std::cout << *zaz;
	std::cout<<hm->getHP()<<std::endl;
	zaz->attack(hm);
	std::cout << *zaz;
	zaz->attack(hm);
	std::cout << *zaz;
	delete zaz;
	delete pr;
	delete pf;
	return 0;
}