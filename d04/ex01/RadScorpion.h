/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 11:19:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 11:21:20 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_REDSCORPION_H
#define D04_REDSCORPION_H

#include "Enemy.h"

class RadScorpion : public Enemy {
public:
	RadScorpion(void);
	~RadScorpion(void);

private:

};

#endif //D04_REDSCORPION_H
