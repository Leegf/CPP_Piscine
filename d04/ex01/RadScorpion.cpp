/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RadScorpion.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 11:19:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 11:24:23 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RadScorpion.h"

RadScorpion::RadScorpion() : Enemy(80, "RadScorpion") {
	std::cout<<"* click click click *"<<std::endl;
}

RadScorpion::~RadScorpion() {
	std::cout<<"* SPROTCH *"<<std::endl;
}

