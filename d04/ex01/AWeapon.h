/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 19:56:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 11:38:16 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_AWEAPON_H
#define D04_AWEAPON_H

#include <iostream>

class AWeapon {
public:
	AWeapon(std::string const & name, int apcost, int damage);
	AWeapon(AWeapon const &src);
	AWeapon &operator=(AWeapon const &rhs);

	virtual ~AWeapon(void);
	std::string getName() const;

	int getAPCost() const;
	int getDamage() const;

	virtual void attack() const = 0;

private:
	std::string _name;
	int _actionPoints;
	int _damage;
	AWeapon(void);
};

#endif //D04_AWEAPON_H
