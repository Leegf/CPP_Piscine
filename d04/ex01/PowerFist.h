/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PowerFist.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 10:30:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 10:50:36 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_POWERFIST_H
#define D04_POWERFIST_H

#include "AWeapon.h"

class PowerFist : public AWeapon {
public:
	PowerFist(void);
	void attack() const;
private:

};

#endif //D04_POWERFIST_H
