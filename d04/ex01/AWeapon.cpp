/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AWeapon.cpp                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 19:56:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/14 20:07:38 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AWeapon.h"

AWeapon::AWeapon(std::string const &name, int apcost, int damage) :
		_name(name), _actionPoints(apcost), _damage(damage)
{ }

AWeapon::AWeapon(void) { }

AWeapon::~AWeapon(void) { }

AWeapon::AWeapon(AWeapon const &src) {
	*this = src;
	return;
}

AWeapon &AWeapon::operator=(AWeapon const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_actionPoints = rhs.getAPCost();
		this->_damage = rhs.getDamage();
	}
	return *this;
}

//getters:

std::string AWeapon::getName() const {
	return this->_name;
}

int AWeapon::getAPCost() const {
	return this->_actionPoints;
}

int AWeapon::getDamage() const {
	return this->_damage;
}
