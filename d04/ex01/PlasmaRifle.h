/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PlasmaRifle.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 10:30:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 10:41:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_PLASMARIFLE_H
#define D04_PLASMARIFLE_H

#include "AWeapon.h"

class PlasmaRifle : public AWeapon {
public:
	PlasmaRifle(void);
	void attack() const;

private:
};

#endif //D04_PLASMARIFLE_H
