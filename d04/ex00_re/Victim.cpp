/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 19:17:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/14 19:25:24 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Victim.h"

Victim::Victim(void) { }

Victim::Victim(std::string name) : _name(name) {
	std::cout<<"Some random victim called "<<this->getName()<<" just popped !"<<std::endl;
}

Victim::~Victim(void) {
	std::cout<<"Victim "<<this->getName()<<" just died for no apparent reason !"<<std::endl;
}

Victim::Victim(Victim const &src) {
	*this = src;
}

Victim &Victim::operator=(Victim const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
	}
	return *this;
}

void Victim::getPolymorphed() const {
	std::cout<<this->getName()<<" has been turned into a cute little sheep !"<<std::endl;
}

//getters:

std::string Victim::getName() const {
	return this->_name;
}

std::ostream &operator<<(std::ostream &o, Victim const &i) {
	o << "I'm "<<i.getName()<<" and I like otters !"<<std::endl;

	return o;
}