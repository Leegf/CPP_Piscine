/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Victim.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 19:17:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/14 19:45:09 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_VICTIM_H
#define D04_VICTIM_H

#include <iostream>

class Victim {
public:

	Victim(Victim const &src);
	Victim(std::string name);
	~Victim(void);
	Victim &operator=(Victim const &rhs);
	std::string getName() const;
	virtual void getPolymorphed() const;

private:
	Victim(void);
	std::string _name;

};

std::ostream &operator<<(std::ostream &o, Victim const &i);


#endif //D04_VICTIM_H
