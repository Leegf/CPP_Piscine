/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 18:59:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/14 19:11:48 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Sorcerer.h"

Sorcerer::Sorcerer(void) {
	return;
}

Sorcerer::Sorcerer(std::string name, std::string title) : _name(name), _title(title) {
	std::cout<<this->getName()<<", "<<this->getTitle()<<", is born !"<<std::endl;
}

Sorcerer::~Sorcerer(void) {
	std::cout<<"I am "<<this->getName()<<", "<<this->getTitle()<<", is dead."
			" Consequences will never be the same !"<<std::endl;
	return;
}

Sorcerer::Sorcerer(Sorcerer const &src) {
	*this = src;
	return;
}

Sorcerer &Sorcerer::operator=(Sorcerer const &rhs) {
	if (this != &rhs) {
		this->_name = rhs.getName();
		this->_title = rhs.getTitle();
	}
	return *this;
}

void Sorcerer::polymorph(Victim const & par) const {
	par.getPolymorphed();
}

//getters:
std::string Sorcerer::getName() const {
	return this->_name;
}

std::string Sorcerer::getTitle() const {
	return this->_title;
}

std::ostream &operator<<(std::ostream &o, Sorcerer const &i) {
	o <<"I am "<<i.getName()<<", "<<i.getTitle()<<
			", and I like ponnies !"<<std::endl;

	return o;
}