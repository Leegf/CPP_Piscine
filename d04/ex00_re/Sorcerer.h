/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Sorcerer.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 18:59:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/14 19:28:31 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_SORCERER_H
#define D04_SORCERER_H

#include <iostream>
#include "Victim.h"

class Sorcerer {
public:
	Sorcerer(Sorcerer const &src);
	Sorcerer(std::string name, std::string title);
	~Sorcerer(void);
	Sorcerer &operator=(Sorcerer const &rhs);
	std::string getName() const;
	std::string getTitle() const;
	void polymorph(Victim const & ) const;

private:
	Sorcerer(void);
	std::string _name;
	std::string _title;
};

std::ostream &operator<<(std::ostream &o, Sorcerer const &i);

#endif //D04_SORCERER_H
