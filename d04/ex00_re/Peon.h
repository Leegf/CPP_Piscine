/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 19:29:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/14 19:45:09 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_PEON_H
#define D04_PEON_H

#include "Victim.h"

class Peon : public Victim {
public:
	Peon(std::string name);
//	Peon(Peon const &src);
	~Peon(void);
	 void getPolymorphed() const;
//	Peon &operator=(Peon const &rhs);
private:
//	Peon(void);
};

//std::ostream &operator<<(std::ostream &o, Peon const &i);

#endif //D04_PEON_H
