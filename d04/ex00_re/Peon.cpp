/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Peon.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/14 19:29:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/14 19:41:31 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Peon.h"

Peon::Peon(std::string name) : Victim(name){
	std::cout<<"Zog zog."<<std::endl;
}
/*Peon::Peon(void) {
	return;
}

 */

Peon::~Peon(void) {
	std::cout<<"Bleuark..."<<std::endl;
}

void Peon::getPolymorphed() const {
	std::cout<<this->getName()<<" has been turned into a pink pony !"<<std::endl;
}

/*
Peon::Peon(Peon const &src) {
	*this = src;
	return;
}

Peon &Peon::operator=(Peon const &rhs) {
	if (this != &rhs)
		this->_foo = rhs.getFoo();
	return *this;
}
*/

/*
std::ostream &operator<<(std::ostream &o, Peon const &i) {
	o << i.getFoo;

	return o;
}*/
