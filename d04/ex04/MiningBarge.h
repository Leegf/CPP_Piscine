/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MiningBarge.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 17:05:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 17:06:08 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_MININGBARGE_H
#define D04_MININGBARGE_H

#include "IMiningLaser.h"

class MiningBarge {
public:
	MiningBarge(void);
	~MiningBarge(void);
	void equip(IMiningLaser* las);
	void mine(IAsteroid * ast) const;
private:
	IMiningLaser ** _lasers;
};

#endif //D04_MININGBARGE_H
