/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AsteroBocal.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 16:52:35 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_ASTEROBOCAL_H
#define D04_ASTEROBOCAL_H

#include "IAsteroid.h"

class AsteroBocal : public IAsteroid {
public:
	AsteroBocal(void);
	AsteroBocal(AsteroBocal const &src);
	~AsteroBocal(void);
	AsteroBocal &operator=(AsteroBocal const &rhs);

	virtual std::string beMined(StripMiner *) const;
	virtual std::string beMined(DeepCoreMiner *) const;
	virtual std::string getName() const;

private:

};

#endif //D04_ASTEROBOCAL_H
