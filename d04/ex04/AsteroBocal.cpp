/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AsteroBocal.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 16:59:06 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AsteroBocal.h"

AsteroBocal::AsteroBocal(void) {
	return;
}

AsteroBocal::~AsteroBocal(void) {
	return;
}

AsteroBocal::AsteroBocal(AsteroBocal const &src) {
	*this = src;
	return;
}

AsteroBocal &AsteroBocal::operator=(AsteroBocal const &rhs) {
	(void)rhs;
	return *this;
}

std::string AsteroBocal::getName() const {
	return ("AsteroBocal");
}

std::string AsteroBocal::beMined(StripMiner *) const {
	return "Flavium";
}

std::string AsteroBocal::beMined(DeepCoreMiner *) const {
	return "Thorite";
}
