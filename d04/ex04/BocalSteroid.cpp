/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BocalSteroid.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 16:52:35 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "BocalSteroid.h"

BocalSteroid::BocalSteroid(void) {
	return;
}

BocalSteroid::~BocalSteroid(void) {
	return;
}

BocalSteroid::BocalSteroid(BocalSteroid const &src) {
	*this = src;
	return;
}

BocalSteroid &BocalSteroid::operator=(BocalSteroid const &rhs) {
	(void)rhs;
	return *this;
}

std::string BocalSteroid::getName() const {
	return "BocalSteroid";
}

std::string BocalSteroid::beMined(StripMiner *) const {
	return "Krpite";
}

std::string BocalSteroid::beMined(DeepCoreMiner *) const {
	return "Zazium";
}
