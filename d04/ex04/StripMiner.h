/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   StripMiner.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:26:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 16:26:13 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_STRIPMINER_H
#define D04_STRIPMINER_H
#include "IMiningLaser.h"

class StripMiner : public IMiningLaser {
public:
	StripMiner(void);
	StripMiner(StripMiner const &src);
	~StripMiner(void);
	StripMiner &operator=(StripMiner const &rhs);

	void mine(IAsteroid * par);
private:

};

#endif //D04_STRIPMINER_H
