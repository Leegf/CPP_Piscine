/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   IMiningLaser.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 15:52:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 16:31:36 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_IMININGLASER_H
#define D04_IMININGLASER_H
#include "IAsteroid.h"
#include <iostream>

class IMiningLaser {
public:
	virtual ~IMiningLaser() {}
	virtual void mine(IAsteroid*) = 0;

};

#endif //D04_IMININGLASER_H
