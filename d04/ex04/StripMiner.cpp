/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   StripMiner.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */ /*   Created: 2018/07/16 16:26:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 17:00:11 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "StripMiner.h"

StripMiner::StripMiner(void) {
	return;
}

StripMiner::~StripMiner(void) {
	return;
}

StripMiner::StripMiner(StripMiner const &src) {
	*this = src;
	return;
}

StripMiner &StripMiner::operator=(StripMiner const &rhs) {
	(void)rhs;
	return *this;
}

void StripMiner::mine(IAsteroid * par) {
	std::cout<<"* strip mining ... got "<<par->beMined(this)<<" ! *"<<std::endl;
}
