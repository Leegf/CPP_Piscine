/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DeepCoreMiner.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:02:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 16:59:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "DeepCoreMiner.h"

DeepCoreMiner::DeepCoreMiner(void) {
	return;
}

DeepCoreMiner::~DeepCoreMiner(void) {
	return;
}

DeepCoreMiner::DeepCoreMiner(DeepCoreMiner const &src) {
	*this = src;
	return;
}

DeepCoreMiner &DeepCoreMiner::operator=(DeepCoreMiner const &rhs) {
	(void)rhs;
	return *this;
}

void DeepCoreMiner::mine(IAsteroid * par) {
	std::cout<<"* mining deep ... got "<<par->beMined(this)<<" ! *"<<std::endl;
}
