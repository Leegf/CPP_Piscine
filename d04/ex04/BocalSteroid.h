/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   BocalSteroid.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 16:45:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_BOCALSTEROID_H
#define D04_BOCALSTEROID_H

#include "IAsteroid.h"

class BocalSteroid : public IAsteroid {
public:
	BocalSteroid(void);
	BocalSteroid(BocalSteroid const &src);
	~BocalSteroid(void);
	BocalSteroid &operator=(BocalSteroid const &rhs);

	virtual std::string beMined(StripMiner *) const;
	virtual std::string beMined(DeepCoreMiner *) const;
	virtual std::string getName() const;

private:

};

#endif //D04_BOCALSTEROID_H
