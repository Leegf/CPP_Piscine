/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 15:50:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 17:16:26 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "DeepCoreMiner.h"
#include "StripMiner.h"
#include "BocalSteroid.h"
#include "AsteroBocal.h"
#include "MiningBarge.h"

int 	main(void) {
	IAsteroid * boc = new BocalSteroid;
	IAsteroid * ast = new AsteroBocal;

	IMiningLaser * strp = new StripMiner;
	IMiningLaser * deep = new DeepCoreMiner;

/*
	strp->mine(boc);
	strp->mine(ast);
	deep->mine(boc);
	deep->mine(ast);
*/

	MiningBarge barge;

	barge.equip(strp);
	barge.equip(strp);
	barge.equip(strp);
	barge.equip(strp);
	barge.equip(strp);
	barge.mine(boc);

	MiningBarge barge2;
	barge2.equip(deep);
	barge2.equip(strp);
	barge2.equip(strp);
	barge2.equip(deep);
	barge2.equip(deep);
	barge2.mine(ast);


	delete boc;
	delete ast;
	delete strp;
	delete deep;
	return (0);
}
