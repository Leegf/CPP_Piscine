/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   DeepCoreMiner.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 16:02:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 16:26:13 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_DEEPCOREMINER_H
#define D04_DEEPCOREMINER_H
#include "IMiningLaser.h"

class DeepCoreMiner : public IMiningLaser {
public:
	DeepCoreMiner(void);
	DeepCoreMiner(DeepCoreMiner const &src);
	~DeepCoreMiner(void);
	DeepCoreMiner &operator=(DeepCoreMiner const &rhs);

	void mine(IAsteroid * par);
private:

};

#endif //D04_DEEPCOREMINER_H
