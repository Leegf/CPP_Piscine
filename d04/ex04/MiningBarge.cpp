/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MiningBarge.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 17:05:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 17:14:51 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MiningBarge.h"

MiningBarge::MiningBarge() {
	this->_lasers = new IMiningLaser*[4];
	for (int i = 0; i < 4 ; i++) {
		this->_lasers[i] = 0;
	}
}

MiningBarge::~MiningBarge() {
	delete this->_lasers;
}

void MiningBarge::equip(IMiningLaser *las) {
	for (int i = 0; i < 4; i++){
		if (!this->_lasers[i]){
			this->_lasers[i] = las;
			return ;
		}
	}
}

void MiningBarge::mine(IAsteroid *ast) const {
		for (int i = 0; i < 4; i++){
			if (this->_lasers[i]){
				this->_lasers[i]->mine(ast);
			}
		}
}
