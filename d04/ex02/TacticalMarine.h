/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 16:16:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 16:27:12 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_TACTICALMARINE_H
#define D04_TACTICALMARINE_H

#include "ISpaceMarine.h"
#include <iostream>

class TacticalMarine : public ISpaceMarine {
public:
	TacticalMarine(void);
	~TacticalMarine(void);
	TacticalMarine* clone() const;
	void battleCry() const;
	void rangedAttack() const;
	void meleeAttack() const;
	TacticalMarine(TacticalMarine const &src);
	TacticalMarine &operator=(TacticalMarine const &rhs);

private:

};

#endif //D04_TACTICALMARINE_H
