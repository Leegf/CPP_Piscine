/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 15:10:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 20:01:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Squad.h"

Squad::Squad(void) : _count(0), _head(0) { }

Squad::~Squad(void) {
	t_unitPtr * tmp;

	while(this->_head)
	{
		tmp = this->_head;
		delete this->_head->content;
		this->_head = this->_head->next;
		delete tmp;
	}
	this->_count = 0;
	this->_head = 0;
}

Squad::Squad(Squad const &src) {
	*this = src;
	return;
}

Squad &Squad::operator=(Squad const &rhs) {
	if (this != &rhs) {
		this->~Squad();
		for(int i = 0; i < rhs.getCount(); i++)
		{
			ISpaceMarine * cur = rhs.getUnit(i)->clone();
			this->push(cur);
		}
	}
	return *this;
}


int Squad::push(ISpaceMarine *par) {
	t_unitPtr * tmp = this->_head;
	t_unitPtr * new_tmp;

	if (!par)
		return this->getCount();
	while (tmp)
	{
		if (tmp->content == par)
			return this->getCount();
		tmp = tmp->next;
	}
	new_tmp = this->newUnit(par);
	if (!(this->_head)) {
		this->_head = new_tmp;
		this->_count++;
		return this->getCount();
	}
	tmp = this->_head;
	while (tmp->next)
		tmp = tmp->next;
	tmp->next=new_tmp;
	this->_count++;
	return this->getCount();
}

t_unitPtr* Squad::newUnit(ISpaceMarine *par) {
	t_unitPtr *tmp = new t_unitPtr;
	tmp->content = par;
	tmp->next = 0;
	return (tmp);
}

//getters:

t_unitPtr* Squad::getHead() const {
	return this->_head;
}

int Squad::getCount() const {
	return this->_count;
}

ISpaceMarine* Squad::getUnit(int N) const {
	t_unitPtr *tmp;
	int i;

	tmp = this->getHead();
	for (i = 0; i < N && tmp; i++) {
		tmp = tmp->next;
	}
	if (i == N)
		return tmp->content;
	else
		return (0);
}
