/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 14:49:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 20:04:13 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Squad.h"
#include "TacticalMarine.h"
#include "AssaultTerminator.h"

int 	main(void){
	ISpaceMarine* bob = new TacticalMarine;
	ISpaceMarine* jim = new AssaultTerminator;
	ISpaceMarine* bob2 = new TacticalMarine;
	ISpaceMarine* bob3 = new TacticalMarine;
	ISpaceMarine* bob4 = new AssaultTerminator;
	ISpaceMarine* bob5 = new AssaultTerminator;
	ISpaceMarine* bob6 = new AssaultTerminator;
	ISpaceMarine* bob7 = new TacticalMarine;
	ISquad* vlc = new Squad;

	vlc->push(bob);
	vlc->push(jim);
	vlc->push(bob2);
	vlc->push(bob3);
	for (int i = 0; i < vlc->getCount(); ++i)
	{
		ISpaceMarine * cur = vlc->getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}
	std::cout<<std::endl;

	Squad * hm = new Squad;
	Squad * hm2 = new Squad;
	hm->push(bob);
	hm->push(jim);
	hm2->push(bob4);
	hm2->push(bob5);
	hm2->push(bob6);
	hm2->push(bob7);

	*hm = *hm2;
	for (int i = 0; i < hm->getCount(); ++i)
	{
		ISpaceMarine * cur = hm->getUnit(i);
		cur->battleCry();
		cur->rangedAttack();
		cur->meleeAttack();
	}
	delete hm;
	delete hm2;
	return (0);
}
