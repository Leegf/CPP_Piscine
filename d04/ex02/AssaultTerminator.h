/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 17:11:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 17:12:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_ASSAULTTERMINATOR_H
#define D04_ASSAULTTERMINATOR_H

#include "ISpaceMarine.h"
#include <iostream>

class AssaultTerminator : public ISpaceMarine {
public:
	AssaultTerminator(void);
	~AssaultTerminator(void);
	AssaultTerminator(AssaultTerminator const &src);
	AssaultTerminator &operator=(AssaultTerminator const &rhs);

	void battleCry() const ;
	AssaultTerminator* clone() const;
	void rangedAttack() const;
	void meleeAttack() const;

private:

};

#endif //D04_ASSAULTTERMINATOR_H
