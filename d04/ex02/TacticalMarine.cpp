/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   TacticalMarine.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 16:16:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 16:30:15 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "TacticalMarine.h"

TacticalMarine::TacticalMarine(void) {
	std::cout<< "Tactical Marine ready for the battle"<<std::endl;
}

TacticalMarine::~TacticalMarine(void) {
	std::cout<<"Aaargh ..."<<std::endl;
}

TacticalMarine* TacticalMarine::clone() const {
	TacticalMarine * hm = new TacticalMarine;

	return (hm);
}

void TacticalMarine::battleCry() const {
	std::cout<<"For the holy PLOT !"<<std::endl;
}

void TacticalMarine::rangedAttack() const {
	std::cout<<"* attacks with bolter *"<<std::endl;
}

void TacticalMarine::meleeAttack() const {
	std::cout<<"* attacks with chainsword *"<<std::endl;
}

TacticalMarine::TacticalMarine(TacticalMarine const &src) {
	*this = src;
}

TacticalMarine &TacticalMarine::operator=(TacticalMarine const &rhs) {
	(void)rhs;
	return *this;
}
