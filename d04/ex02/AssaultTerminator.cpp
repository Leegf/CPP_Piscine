/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AssaultTerminator.cpp                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 17:11:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/15 17:11:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AssaultTerminator.h"

AssaultTerminator::AssaultTerminator(void) {
	std::cout<<"* teleports from space *"<<std::endl;
}

AssaultTerminator::~AssaultTerminator(void) {
	std::cout<<"I'll be back ..."<<std::endl;
}

AssaultTerminator::AssaultTerminator(AssaultTerminator const &src) {
	*this = src;
	return;
}

AssaultTerminator* AssaultTerminator::clone() const {
	AssaultTerminator * hm = new AssaultTerminator;

	return (hm);
}

AssaultTerminator &AssaultTerminator::operator=(AssaultTerminator const &rhs) {
	(void)rhs;
	return *this;
}

void AssaultTerminator::battleCry() const {
	std::cout<<"This code is unclean. PURIFY IT !"<<std::endl;
}

void AssaultTerminator::rangedAttack() const {
	std::cout<<"* does nothing *"<<std::endl;
}

void AssaultTerminator::meleeAttack() const {
	std::cout<<"* attacks with chainfists *"<<std::endl;
}
