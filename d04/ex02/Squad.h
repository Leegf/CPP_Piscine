/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Squad.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 15:10:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 11:44:11 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_SQUAD_H
#define D04_SQUAD_H

#include "ISquad.h"

struct t_unitPtr {
	ISpaceMarine * content;
	t_unitPtr * next;
};

class Squad : public ISquad {
public:
	Squad(void);
	~Squad(void);
	Squad(Squad const &src);
	virtual Squad &operator=(Squad const &rhs);
	int getCount() const;
	t_unitPtr * getHead() const;
	ISpaceMarine * getUnit(int N) const ;
	int push(ISpaceMarine* par);
private:
	int _count;
	t_unitPtr * _head;
	t_unitPtr * newUnit(ISpaceMarine * par);
};

#endif //D04_SQUAD_H
