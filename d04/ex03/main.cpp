/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 20:09:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 13:28:03 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "IMateriaSource.h"
#include "Cure.h"
#include "Ice.h"
#include "MateriaSource.h"
#include "Character.h"

int main() {
	IMateriaSource* src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());
	ICharacter* zaz = new Character("zaz");
	AMateria* tmp;
	tmp = src->createMateria("ice");
	zaz->equip(tmp);
	tmp = src->createMateria("cure");
	zaz->equip(tmp);
	tmp = src->createMateria("ice");
	zaz->equip(tmp);
	ICharacter* bob = new Character("bob");
	bob->equip(tmp);
	*zaz = *bob;
	zaz->use(0, *bob); //gotta work
	zaz->use(1, *bob); //gotta work
	zaz->use(2, *bob);//gotta work
	zaz->use(3, *bob);
	zaz->unequip(2);
	zaz->use(2, *bob);
	zaz->use(1, *bob); //gotta work
	zaz->unequip(1);
	zaz->use(1, *bob);

	delete bob;
	delete zaz;
	delete src;
	return 0;
}
