/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 12:10:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 13:26:55 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Character.h"

Character::Character(void) {

}

Character::Character(std::string name) : _name(name) {
	this->_inventory = new AMateria*[4];
	for (int i = 0; i < 4; i++){
		this->_inventory[i] = 0;
	}
}

Character::~Character(void) {
	for (int i = 0; i < 4; i++) {
		if (!this->_inventory[i]) {
			delete this->_inventory[i];
		}
	}
	delete this->_inventory;
}

Character &Character::operator=(Character const &rhs) {
	if (this != &rhs) {
		this->~Character();
		this->_inventory = new AMateria*[4];
		this->_name = rhs.getName();
		for (int i = 0; i < 4; i++){
			this->_inventory[i] = rhs.getInventory()[i]->clone();
		}
	}
	return *this;
}

Character::Character(Character const &src) {
	*this = src;
	return;
}

void Character::equip(AMateria *m) {
	for (int i = 0; i < 4; i++) {
		if (!this->_inventory[i]) {
			this->_inventory[i] = m;
			return ;
		}
	}
}

void Character::unequip(int idx) {
	if (idx >= 0 && idx < 4)
		this->_inventory[idx] = 0;
}

void Character::use(int idx, ICharacter &target) {
	if (idx >=0 && idx < 4 && this->_inventory[idx]) {
		this->_inventory[idx]->use(target);
	}
}

//getters:
std::string const & Character::getName() const {
	return this->_name;
}

AMateria ** Character::getInventory() const {
	return this->_inventory;
}
