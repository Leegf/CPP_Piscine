/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 12:50:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 13:08:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "MateriaSource.h"

MateriaSource::MateriaSource(void) {
	this->_inventory = new AMateria*[4];
	for (int i = 0; i < 4; i++){
		this->_inventory[i] = 0;
	}
}

MateriaSource::~MateriaSource(void) {
	for (int i = 0; i < 4; i++) {
		if (!this->_inventory[i]) {
			delete this->_inventory[i];
			this->_inventory[i] = 0;
		}
	}
	delete this->_inventory;
}

void MateriaSource::learnMateria(AMateria * par) {
	for (int i = 0; i < 4; i++) {
		if (!this->_inventory[i]) {
			this->_inventory[i] = par;
			return ;
		}
	}
}

AMateria* MateriaSource::createMateria(std::string const &type) {
	for (int i = 0; i < 4; i++){
		if (this->getInventory()[i]->getType() == type)
			return this->_inventory[i]->clone();
	}
	return (0);
}

MateriaSource::MateriaSource(MateriaSource const &src) {
	*this = src;
	return;
}

MateriaSource &MateriaSource::operator=(MateriaSource const &rhs) {
	if (this != &rhs) {
		this->~MateriaSource();
		this->_inventory = new AMateria*[4];
		for (int i = 0; i < 4; i++){
			this->_inventory[i] = rhs.getInventory()[i]->clone();
		}
	}
	return *this;
}

//getters:
AMateria ** MateriaSource::getInventory() const {
	return this->_inventory;
}
