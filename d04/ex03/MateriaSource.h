/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   MateriaSource.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 12:50:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 13:07:42 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_MATERIASOURCE_H
#define D04_MATERIASOURCE_H

#include "IMateriaSource.h"
#include "AMateria.h"

class MateriaSource : public IMateriaSource {
public:
	MateriaSource(void);
	~MateriaSource(void);
	virtual void learnMateria(AMateria* par);
	virtual AMateria* createMateria(std::string const & type);

	MateriaSource(MateriaSource const &src);
	MateriaSource &operator=(MateriaSource const &rhs);
	AMateria ** getInventory() const;

private:
	AMateria ** _inventory;
};

#endif //D04_MATERIASOURCE_H
