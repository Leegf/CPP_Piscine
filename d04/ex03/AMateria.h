/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 20:17:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 11:55:44 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_AMATERIA_H
#define D04_AMATERIA_H

#include <iostream>
#include "ICharacter.h"

class AMateria {
public:
	AMateria(std::string const & type);
	virtual ~AMateria(void);
	AMateria(AMateria const &src);
	AMateria &operator=(AMateria const &rhs);

	std::string const & getType() const; //Returns the materia type
	unsigned int getXP() const; //Returns the Materia's XP

	virtual AMateria* clone() const = 0;
	virtual void use(ICharacter& target);

private:
	AMateria(void);
	unsigned int _xp;
	std::string _type;
};

#endif //D04_AMATERIA_H
