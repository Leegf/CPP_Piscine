/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Ice.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 11:55:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 11:58:32 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_ICE_H
#define D04_ICE_H

#include "AMateria.h"

class Ice : public AMateria {
public:
	Ice(void);
	~Ice(void);
	Ice * clone() const;

private:

};

#endif //D04_ICE_H
