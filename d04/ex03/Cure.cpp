/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 11:58:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 12:05:55 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Cure.h"

Cure::Cure(void) : AMateria("cure") { }

Cure::~Cure(void) { }

Cure* Cure::clone() const {
	return (new Cure);
}

