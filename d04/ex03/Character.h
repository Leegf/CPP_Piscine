/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Character.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 12:10:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 15:34:37 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_CHARACTER_H
#define D04_CHARACTER_H

#include <iostream>
#include "AMateria.h"

class Character : public ICharacter {
public:
	Character(std::string name);
	Character(Character const &src);
	~Character(void);
	Character &operator=(Character const &rhs);

	void equip(AMateria * m);
	void unequip(int idx);
	void use(int idx, ICharacter & target);

	std::string const & getName() const;
	AMateria ** getInventory() const;

private:
	Character(void);
	std::string _name;
	AMateria ** _inventory;
};

#endif //D04_CHARACTER_H
