/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   AMateria.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/15 20:17:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 11:55:44 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "AMateria.h"

AMateria::AMateria(void) {
	return;
}

AMateria::AMateria(std::string const &type) : _xp(0), _type(type) {

}

AMateria::~AMateria(void) {
	return;
}

AMateria::AMateria(AMateria const &src) {
	*this = src;
	return;
}

AMateria &AMateria::operator=(AMateria const &rhs) {
	if (this != &rhs)
		this->_xp = rhs.getXP();
	return *this;
}

void AMateria::use(ICharacter &target) {
	if (this->getType() == "ice")
		std::cout<<"* shoots an ice bolt at "<<target.getName()<<" *";
	else if (this->getType() == "cure")
		std::cout<<"* heals "<<target.getName()<<"'s wounds *";
	std::cout<<std::endl;
	this->_xp+= 10;
}

//getters:

unsigned int AMateria::getXP() const {
	return this->_xp;
}

std::string const& AMateria::getType() const {
	return this->_type;
}
