/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Cure.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/07/16 11:58:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/07/16 11:58:32 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D04_CURE_H
#define D04_CURE_H

#include "AMateria.h"

class Cure : public AMateria {
public:
	Cure(void);
	~Cure(void);
	Cure * clone() const;

private:

};

#endif //D04_CURE_H
