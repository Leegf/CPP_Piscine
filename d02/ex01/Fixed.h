/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 18:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 19:58:33 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D02_FIXED_H
#define D02_FIXED_H
#include <cmath>
#include <iostream>

class Fixed {
public:
	Fixed(void); //constructor
	Fixed(Fixed const &src); //copy constructor
	~Fixed(void); //destructor
	Fixed (const int par); //constructor for constant integer
	Fixed (const float par); //constructor for constant float
	Fixed &operator=(Fixed const &rhs); //assignation operator overloaded.
	int getRawBits( void ) const;
	void setRawBits(int const raw );
	float toFloat(void) const;
	int toInt(void) const;
private:
	int _val;
	static const int _fracBits;
};

std::ostream & operator<<(std::ostream & o, Fixed const & i);

#endif //D02_FIXED_H
