/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 18:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 21:10:47 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.h"
#include <iostream>

Fixed::Fixed(void) : _val(0) {
	std::cout<<"Default constructor called"<<std::endl;
	return;
}

Fixed::~Fixed() {
	std::cout<<"Destructor called"<<std::endl;
}

Fixed::Fixed(Fixed const &src) {
	std::cout<<"Copy constructor called"<<std::endl;
	*this = src;
	return;
}

Fixed & Fixed::operator=(Fixed const &rhs) {
	std::cout<<"Assignation operator called"<<std::endl;
	if (this != &rhs)
		this->_val = rhs.getRawBits();
	return *this;
}

Fixed::Fixed(const int par) {
	std::cout<<"Int constructor called"<<std::endl;
	setRawBits(par << Fixed::_fracBits);
}

Fixed::Fixed(const float par) {
	std::cout<<"Float constructor called"<<std::endl;
	setRawBits(roundf(par * (1 << Fixed::_fracBits)));
}

void Fixed::setRawBits(int const raw)
{
	this->_val = raw;
}

float Fixed::toFloat() const {
	return (float)this->_val  / (1 << Fixed::_fracBits);
}

int Fixed::toInt() const {
	return this->_val >> Fixed::_fracBits;
}

int Fixed::getRawBits() const {
	return this->_val;
}

const int Fixed::_fracBits = 8;

std::ostream & operator<<(std::ostream & o, Fixed const & i) {
	o<<i.toFloat();

	return o;
}
