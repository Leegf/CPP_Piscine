/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 18:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 18:53:09 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D02_FIXED_H
#define D02_FIXED_H


class Fixed {
public:
	Fixed(void); //constructor
	Fixed(Fixed const &src); //copy constructor
	~Fixed(void); //destructor
	Fixed &operator=(Fixed const &rhs); //assignation operator overloaded.
	int getRawBits( void ) const;
	void setRawBits(int const raw );
private:
	int _val;
	static const int _fracBits;
};


#endif //D02_FIXED_H
