/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 18:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 19:32:18 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.h"
#include <iostream>

Fixed::Fixed(void) : _val(0) {
	std::cout<<"Default constructor called"<<std::endl;
	return;
}

Fixed::~Fixed() {
	std::cout<<"Destructor called"<<std::endl;
}

Fixed::Fixed(Fixed const &src) {
	std::cout<<"Copy constructor called"<<std::endl;
	*this = src;
	return;
}

Fixed & Fixed::operator=(Fixed const &rhs) {
	std::cout<<"Assignation operator called"<<std::endl;
	if (this != &rhs)
		this->_val = rhs.getRawBits();
	return *this;
}

void Fixed::setRawBits(int const raw)
{
	this->_val = raw;
}

int Fixed::getRawBits() const {
	std::cout<<"getRawBits member function called"<<std::endl;
	return this->_val;
}

const int Fixed::_fracBits = 8;
