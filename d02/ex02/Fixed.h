/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 18:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 16:13:31 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D02_FIXED_H
#define D02_FIXED_H
#include <cmath>
#include <iostream>

class Fixed {
public:
	Fixed(void); //constructor
	Fixed(Fixed const &src); //copy constructor
	~Fixed(void); //destructor
	Fixed (const int par); //constructor for constant integer
	Fixed (const float par); //constructor for constant float
	Fixed &operator=(Fixed const &rhs); //assignation operator overloaded.
	int getRawBits( void ) const;
	void setRawBits(int const raw );
	float toFloat(void) const;
	int toInt(void) const;
	static Fixed & min(Fixed & par1, Fixed &par2);
	static Fixed & max(Fixed & par1, Fixed &par2);
	static Fixed const & min(Fixed const & par1, Fixed const &par2);
	static Fixed const & max(Fixed const & par1, Fixed const &par2);

	//overload operators:
	bool operator>(Fixed const & par) const;
	bool operator<(Fixed const & par) const;
	bool operator>=(Fixed const & par) const;
	bool operator<=(Fixed const & par) const;
	bool operator==(Fixed const & par) const;
	bool operator!=(Fixed const & par) const;
	Fixed operator+(Fixed const & rhs) const;
	Fixed operator-(Fixed const & rhs) const;
	Fixed operator*(Fixed const & rhs) const;
	Fixed operator/(Fixed const & rhs) const;
	Fixed operator++(int);
	Fixed &operator++();
	Fixed operator--(int);
	Fixed &operator--();
private:
	int _val;
	static const int _fracBits;

};

std::ostream & operator<<(std::ostream & o, Fixed const & i);

#endif //D02_FIXED_H
