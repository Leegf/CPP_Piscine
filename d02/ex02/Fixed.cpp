/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Fixed.cpp                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 18:45:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 16:23:56 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Fixed.h"
#include <cstdlib>

Fixed::Fixed(void) : _val(0) {
	std::cout<<"Default constructor called"<<std::endl;
	return;
}

Fixed::~Fixed() {
	std::cout<<"Destructor called"<<std::endl;
}

Fixed::Fixed(Fixed const &src) {
	std::cout<<"Copy constructor called"<<std::endl;
	*this = src;
	return;
}

Fixed & Fixed::operator=(Fixed const &rhs) {
	std::cout<<"Assignation operator called"<<std::endl;
	if (this != &rhs)
		this->_val = rhs.getRawBits();
	return *this;
}

Fixed::Fixed(const int par) {
	std::cout<<"Int constructor called"<<std::endl;
	setRawBits(par << Fixed::_fracBits);
}

Fixed::Fixed(const float par) {
	std::cout<<"Float constructor called"<<std::endl;
	setRawBits(roundf(par * (1 << Fixed::_fracBits)));
}

void Fixed::setRawBits(int const raw)
{
	this->_val = raw;
}

float Fixed::toFloat() const {
	return (float)this->_val  / (1 << Fixed::_fracBits);
}

int Fixed::toInt() const {
	return this->_val >> Fixed::_fracBits;
}

int Fixed::getRawBits() const {
	return this->_val;
}

Fixed & Fixed::min(Fixed &par1, Fixed &par2) {
	return par1 < par2 ? par1 : par2;
}

Fixed & Fixed::max(Fixed &par1, Fixed &par2) {
	return par1 < par2 ? par2 : par1;
}

Fixed const & Fixed::max(Fixed const &par1, Fixed const &par2) {
	return par1 < par2 ? par2 : par1;
}

Fixed const & Fixed::min(Fixed const &par1, Fixed const &par2) {
	return par1 < par2 ? par1 : par2;
}

const int Fixed::_fracBits = 8;

//Operators:

std::ostream & operator<<(std::ostream & o, Fixed const & i) {
	o<<i.toFloat();

	return o;
}

bool Fixed::operator>(Fixed const & par2) const
{
	if (this->getRawBits() > par2.getRawBits())
		return (true);
	return (false);
}

bool Fixed::operator<(Fixed const & par2) const
{
	if (this->getRawBits() < par2.getRawBits())
		return (true);
	return (false);
}

bool Fixed::operator<=(Fixed const & par2) const
{
	if (this->getRawBits() <= par2.getRawBits())
		return (true);
	return (false);
}

bool Fixed::operator>=(Fixed const & par2) const
{
	if (this->getRawBits() >= par2.getRawBits())
		return (true);
	return (false);
}

bool Fixed::operator==(Fixed const & par2) const
{
	if (this->getRawBits() == par2.getRawBits())
		return (true);
	return (false);
}

bool Fixed::operator!=(Fixed const & par2) const
{
	if (this->getRawBits() != par2.getRawBits())
		return (true);
	return (false);
}

Fixed Fixed::operator+(Fixed const &rhs) const {
	return Fixed(this->toFloat() + rhs.toFloat());
}

Fixed Fixed::operator-(Fixed const &rhs) const {
	return Fixed(this->toFloat() - rhs.toFloat());
}

Fixed Fixed::operator*(Fixed const &rhs) const {
	return Fixed(this->toFloat() * rhs.toFloat());
}

Fixed Fixed::operator/(Fixed const &rhs) const {
	if (rhs.toFloat() == 0) {
		std::cout << "You can't divide by zero.";
		exit(1);
	}
	return Fixed(this->toFloat() / rhs.toFloat());
}

Fixed Fixed::operator++(int) {
	Fixed tmp(*this);

	this->setRawBits(this->getRawBits() + 1);
	return tmp;
}

Fixed &Fixed::operator++() {
	this->setRawBits(this->getRawBits() + 1);
	return *this;
}

Fixed Fixed::operator--(int) {
	Fixed tmp(*this);

	this->setRawBits(this->getRawBits() - 1);
	return tmp;
}

Fixed &Fixed::operator--() {
	this->setRawBits(this->getRawBits() - 1);
	return *this;
}
