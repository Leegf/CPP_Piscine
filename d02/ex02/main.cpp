/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 21:14:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/21 16:26:16 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Fixed.h"

int 	main(void) {

	Fixed a(3);
/*	Fixed const b( Fixed(5.05f) * Fixed(2));
	std::cout<<"< "<<((a < b) ? "true" : "false")<<std::endl;
	std::cout<<"> "<<((a > b) ? "true" : "false")<<std::endl;
	std::cout<<">= "<<((a >= b) ? "true" : "false")<<std::endl;
	std::cout<<"<= "<<((a <= b) ? "true" : "false")<<std::endl;
	std::cout<<"== "<<((a == b) ? "true" : "false")<<std::endl;
	std::cout<<"!= "<<((a != b) ? "true" : "false")<<std::endl;
	std::cout<<"+ "<<(a + b)<<std::endl;
	std::cout<<"- "<<(a - b)<<std::endl;
	std::cout<<"* "<<(a * b)<<std::endl;
	std::cout<<"/ "<<(a / b)<<std::endl;*/
	std::cout << a << std::endl;
	std::cout <<++a << std::endl;
	std::cout << a << std::endl;
	std::cout << a++ << std::endl;
	std::cout << a << std::endl;
//	std::cout << b << std::endl;
//	std::cout << Fixed::max( a, b ) << std::endl
	return 0;
}
