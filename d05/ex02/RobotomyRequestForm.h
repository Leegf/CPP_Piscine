/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 19:13:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 19:21:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D05_ROBOTOMYREQUESTFORM_H
#define D05_ROBOTOMYREQUESTFORM_H

#include <iostream>
#include "Form.h"

class RobotomyRequestForm : public Form {
public:
	RobotomyRequestForm(void);
	RobotomyRequestForm(std::string target);
	RobotomyRequestForm(RobotomyRequestForm const &src);
	~RobotomyRequestForm(void);
	RobotomyRequestForm &operator=(RobotomyRequestForm const &rhs);
	std::string getName(void) const;
	int getGradToSign(void) const;
	int getGradToEx(void) const;
	void action(void) const;

private:

};

std::ostream &operator<<(std::ostream &o, RobotomyRequestForm const &i);


#endif //D05_ROBOTOMYREQUESTFORM_H
