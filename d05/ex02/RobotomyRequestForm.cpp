/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RobotomyRequestForm.cpp                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 19:13:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 19:21:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RobotomyRequestForm.h"

RobotomyRequestForm::RobotomyRequestForm(void) {
	return;
}

RobotomyRequestForm::RobotomyRequestForm(std::string target) {
	this->setTarget(target);
}

RobotomyRequestForm::~RobotomyRequestForm(void) {
	return;
}

RobotomyRequestForm::RobotomyRequestForm(RobotomyRequestForm const &src) {
	*this = src;
	return;
}

RobotomyRequestForm &
RobotomyRequestForm::operator=(RobotomyRequestForm const &rhs) {
	if (this != &rhs)
		this->setTarget(rhs.getTarget());
	return *this;
}

std::ostream &operator<<(std::ostream &o, RobotomyRequestForm const &i) {
	o << i.getTarget();

	return o;
}

void RobotomyRequestForm::action() const {
	srand(clock());
	std::cout<<"BBZBZNNBZNBZBZBZBZBZBZBZBZBZBZB"<<std::endl;
	if (rand() % 100 < 50)
		std::cout<<"<"<<this->getTarget()<<"> has been robotomized successfully."<<std::endl;
	else
		std::cout<<"<"<<this->getTarget()<<">'s robotomizing ended up in a failure."<<std::endl;
}

//getters:
std::string RobotomyRequestForm::getName() const {
	return "Robotomy Request";
}

int RobotomyRequestForm::getGradToEx() const {
	return 45;
}

int RobotomyRequestForm::getGradToSign() const {
	return 72;
}
