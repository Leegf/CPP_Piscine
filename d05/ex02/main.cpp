/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 10:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 19:36:05 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.h"
#include "Form.h"
#include "ShrubberyCreationForm.h"
#include "RobotomyRequestForm.h"
#include "PresidentialPardonForm.h"

int main(void){
	Bureaucrat sir("Bob", 137);
	Bureaucrat sir2("Joe", 138);

	ShrubberyCreationForm hm("granny");
	hm.beSigned(sir);
	std::cout<<hm;
	hm.execute(sir); //executing normally
	hm.execute(sir2); //executing when grade is too low
	sir2.setGrade(1); //making okay grade

	hm.setSignedFlag(false);
	hm.execute(sir2); //trying to execute again when the form is unsigned.
	hm.setSignedFlag(true); //changing signed flag to true;
	hm.beSigned(sir2); //making it signed once again.
	hm.execute(sir2); //trying to execute when the form is signed.

	std::cout<<"testing executeform"<<std::endl;
	Bureaucrat test("Leo", 133);
	test.executeForm(hm); //shows that everything went okay.
	test.setGrade(150);
	test.executeForm(hm); //nope
	test.setGrade(20);
	hm.setSignedFlag(false);
	test.executeForm(hm); //nope
	std::cout<<"Robotomy:"<<std::endl<<std::endl;
	RobotomyRequestForm rob("Granpda");
	Bureaucrat forRob("Silly", 3);
	forRob.executeForm(rob); //form is not signed.
	rob.beSigned(forRob);
	forRob.executeForm(rob); //form is signed.
	forRob.setGrade(100);
	forRob.executeForm(rob); //grade is unsufficiened.
	std::cout<<"Presidnetial:"<<std::endl<<std::endl;
	PresidentialPardonForm trump("Birdy");
	Bureaucrat forPrez("Omg", 24);
	forPrez.executeForm(trump); //not signed
	trump.beSigned(forPrez);
	forPrez.executeForm(trump); //grade is not enough
	forPrez.setGrade(5);
	forPrez.executeForm(trump); //ok
	trump.execute(forPrez); //ok
	return (0);
}
