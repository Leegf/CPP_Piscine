# nested classes.

```
class Cat
{
	public:
	class Leg
	{
		//[...]
	};
};

class Dog
{
	public class Leg
	{
		//[...]
	};
};
```

instanciating is like this:
```
int main() 
{
	Cat somecat;
	Cat::Leg somecatsleg;
}
```

try and catch
```
void test1()
{
	try
	{
		//do some studff
		if (error)
		{
			throw std::exception();
		}
		else
		{
			//Do some more stuff
		}
	}
	catch(std::exception e)
	{

	}
}

void test2()
{
	//do stuff
	if (error)
	{
		throw std::exception();
	}
	else
	{
		//Do some more stuff
	}
}

void test3()
{
	try
	{
		test2();
	}
	catch (std::exception& e)
	{
		//Handle error
	}
}

void test4()
{
	class PEBKACException : public std::exception
	{
		public:
		virtual const char* what() const throw()
		{
			return ("Problem exists beetween keyboard and char"):
		}
	};
	try
	{
		test(3);
	}
	catch(PEBKACException& e)
	{
		//Handle the fact the user is idiot.
	}
	catch(std::exception& e)
	{
		//Handle other exceptions that are like std::exception.
	}
}
```
