/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 10:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 12:47:46 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.h"

int main(void){
	Bureaucrat hm;
	hm.setGrade(4);
	for (int i = 0; i < 5; i++) {
		hm.incrementGrade();
		std::cout<<hm;
	}
	hm.setGrade(146);
	for (int i = 0; i < 6; i++) {
		hm.decrementGrade();
		std::cout<<hm;
	}

	Bureaucrat hm2("Fridrih", 142);
	std::cout<<hm2;
	Bureaucrat hm3("Joe", -832);
	std::cout<<hm3;
	Bureaucrat hm4("Bob", 151);
	std::cout<<hm4;
	return (0);
}
