/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 21:08:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 21:33:09 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "OfficeBlock.h"
#include "Intern.h"
#include "Form.h"
#include "Bureaucrat.h"

OfficeBlock::OfficeBlock(void) : _inter(NULL), _sign(NULL), _ex(NULL) {
	return;
}

OfficeBlock::~OfficeBlock(void) {
	return;
}

void OfficeBlock::setIntern(Intern *intern) {
	this->_inter = intern;
}

void OfficeBlock::setExecutor(Bureaucrat *ex) {
	this->_ex = ex;
}

void OfficeBlock::setSigner(Bureaucrat *sign) {
	this->_sign = sign;
}

void OfficeBlock::doBureaucracy(std::string FormName, std::string targetName) {
		if (!this->_sign || !this->_ex || !this->_sign )
			throw NotFullOfficeException();
		Form *job;
		job = this->_inter->makeForm(FormName, targetName);
	std::cout<<"Some random intern started working upon <"<<FormName<<">"<<std::endl;
		job->beSigned(*(this->_sign));
	std::cout<<"<"<<this->_sign->getName()<<"> signed it."<<std::endl;
		this->_ex->executeForm(*job);
	std::cout<<"<"<<this->_ex->getName()<<"> executed it."<<std::endl;
}

const char* OfficeBlock::NotFullOfficeException::what() const throw() {
	return ("You can't work without enough employees!");
}
