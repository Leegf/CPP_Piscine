/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 17:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 17:41:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D05_SHRUBBERYCREATIONFORM_H
#define D05_SHRUBBERYCREATIONFORM_H
#include "Form.h"

class ShrubberyCreationForm : public Form {
public:
	ShrubberyCreationForm(void);
	ShrubberyCreationForm(std::string target);
	ShrubberyCreationForm(ShrubberyCreationForm const &src);
	ShrubberyCreationForm &operator=(ShrubberyCreationForm const &rhs);
	~ShrubberyCreationForm(void);
 	std::string getName(void) const;
	int getGradToSign(void) const;
	int getGradToEx(void) const;
	void action(void) const;

//	ShrubberyCreationForm &operator=(ShrubberyCreationForm const &rhs);

private:
};

#endif //D05_SHRUBBERYCREATIONFORM_H
