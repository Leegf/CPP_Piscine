/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 10:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 21:35:17 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.h"
#include "Bureaucrat.h"
#include "OfficeBlock.h"

int main(void){
	OfficeBlock ob;

	Intern idiot;
	Bureaucrat hermes = Bureaucrat("Hermes Conrad", 37);
	Bureaucrat bob = Bureaucrat("Bobby Bobson", 123);

	ob.setIntern(&idiot);
	ob.setSigner(&hermes);
//	ob.setExecutor(&bob);

	try {
		ob.doBureaucracy("Robotomy Request", "Granny");
	}
	catch(OfficeBlock::NotFullOfficeException & e) {
		std::cout<<"caught an exception: "<<e.what()<<std::endl;
	}
	catch(std::exception & e) {
		std::cout<<"caught standard exception."<<std::endl;
	}
	return (0);
}
