/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 19:40:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 20:31:19 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.h"

Intern::Intern(void) {
	return;
}

Intern::~Intern(void) {
	return;
}

Intern::Intern(Intern const &src) {
	*this = src;
	return;
}

Intern &Intern::operator=(Intern const &rhs) {
	if (this != &rhs) {
		this->_formName = rhs.getFormName();
		this->_targetName = rhs.getTargetName();
	}
	return *this;
}

Form * Intern::makeForm(std::string formName, std::string targetName) {
	try {
		this->_formName = formName;
		this->_targetName = targetName;
		if (formName == "Robotomy Request") {
			return new RobotomyRequestForm(this->getTargetName());
		}
		else if (formName == "Presidential Pardon") {
			return new PresidentialPardonForm(this->getTargetName());
		}
		else if (formName == "Shrubbery Creation")
			return new ShrubberyCreationForm(this->getTargetName());
		else
			throw WrongRequestException();
	}
	catch (WrongRequestException & e) {
		std::cout<<"caught exception: "<<e.what()<<std::endl;
		exit(1);
	}
	catch(std::exception & e) {
		std::cout<<"standard exception caught."<<std::endl;
	}
	return (NULL);
}

//getters:
std::string Intern::getTargetName() const {
	return this->_targetName;
}

std::string Intern::getFormName() const {
	return this->_formName;
}

//exceptions:

const char* Intern::WrongRequestException::what() const throw() {
	return ("Wrong name of the form");
}
