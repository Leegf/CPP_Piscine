/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 10:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 18:24:32 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.h"
#include "Form.h"

Bureaucrat::Bureaucrat(void) :  _name("Alfred"), _grade(2) {
	return;
}

Bureaucrat::Bureaucrat(const std::string nameOfB, int grade) : _name(nameOfB), _grade(150) {
	//by default it is 150.
	try {
		if (grade > 150)
			throw GradeTooLowException();
		else if (grade < 1)
			throw GradeTooHighException();
		else
			this->_grade = grade;
	}
	catch(GradeTooHighException & e) {
		std::cout<<"Exception caught: "<<e.what()<<std::endl;
	}
	catch(GradeTooLowException & e) {
		std::cout << "Exception caught: " << e.what() << std::endl;
	}
	catch (std::exception & e) {
		std::cout << "Standard exception caught" << std::endl;
	}
}

Bureaucrat::~Bureaucrat(void) {
	return;
}

Bureaucrat::Bureaucrat(Bureaucrat const &src) {
	*this = src;
	return;
}

Bureaucrat &Bureaucrat::operator=(Bureaucrat const &rhs) {
	if (this != &rhs) {
		this->_grade = rhs.getGrade();
	}
	return *this;
}

void Bureaucrat::signForm(Form &par) {
	if (this->getGrade() <= par.getGradToSign()) {
		std::cout<<"<"<<this->getName()<<"> signs <"<<par.getName()<<">"<<std::endl;
		par.signedFlag = true;
	}
	else
		std::cout<<"<"<<this->getName()<<"> cannot sign <"<<par.getName()<<"> because "<<"<"
 <<par.getName()<<"> requires <"<<par.getGradToSign()<<"> grade."<<std::endl;

}

void Bureaucrat::incrementGrade() {
	try {
		if (this->getGrade() - 1 < 1)
			throw GradeTooHighException();
		else
			this->_grade--;
	}
	catch(GradeTooHighException & e) {
		std::cout<<"Exception caught: "<<e.what()<<std::endl;
	}
	catch (std::exception & e) {
		std::cout<<"Standard exception caught"<<std::endl;
	}
}

void Bureaucrat::decrementGrade() {
	try {
		if (this->getGrade() + 1 > 150)
			throw GradeTooLowException();
		else
			this->_grade++;
	}
	catch (GradeTooLowException & e) {
		std::cout<<"Exception caught: "<<e.what()<<std::endl;
	}
	catch (std::exception & e) {
		std::cout<<"Standard exception caught"<<std::endl;
	}
}

void Bureaucrat::executeForm(Form const &form) {
	if (form.execute(*this)) {
		std::cout<<"<"<<this->getName()<<"> executes <"<<form.getName()<<">"<<std::endl;
	}
	else {
		std::cout<<"<"<<this->getName()<<"> couldn't execute <"<<form.getName()<<"> because of reasons showed above "<<std::endl;
	}
}

const char* Bureaucrat::GradeTooHighException::what() const throw() {
	return ("WHOOPS, grade is already the highest.");
}

const char* Bureaucrat::GradeTooLowException::what() const throw() {
	return ("WHOOPS, grade can't be more low than that.");
}

//setters:

void Bureaucrat::setGrade(int gr) {
	this->_grade = gr;
}
//getters:

int Bureaucrat::getGrade() const {
	return this->_grade;
}

std::string Bureaucrat::getName() const {
	return this->_name;
}

std::ostream & operator<<(std::ostream & o, Bureaucrat const & i) {
	o<<"<"<<i.getName()<<">, bureaucrat, grade <"<<i.getGrade()<<">"<<std::endl;

	return o;
}
