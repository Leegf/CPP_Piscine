/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   OfficeBlock.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 21:08:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 21:27:44 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D05_OFFICEBLOCK_H
#define D05_OFFICEBLOCK_H
#include <iostream>

class Intern;
class Bureaucrat;

class OfficeBlock {
public:
	OfficeBlock(void);
	OfficeBlock(Intern * inter, Bureaucrat * sign, Bureaucrat * ex);
	~OfficeBlock(void);
	void setIntern (Intern * intern);
	void setSigner(Bureaucrat * sign);
	void setExecutor(Bureaucrat * ex);
	void doBureaucracy(std::string FormName, std::string targetName);
	class NotFullOfficeException : public std::exception {
	public:
		virtual const char* what() const throw();
	};

private:
	Intern * _inter;
	Bureaucrat * _sign;
	Bureaucrat * _ex;
};

#endif //D05_OFFICEBLOCK_H
