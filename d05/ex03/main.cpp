/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 10:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 20:35:50 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Intern.h"
#include "Bureaucrat.h"

int main(void){
	Intern someRandomIntern;
	Intern SuperIntern;
	Intern SuperPuperIntern;
	Intern CoolIntern;

	Form* rrf;
	Bureaucrat tmp("alfred", 1);

	rrf = someRandomIntern.makeForm("Robotomy Request", "Bender");
	rrf->beSigned(tmp);
	rrf->execute(tmp);

	Form * rrf2 = SuperIntern.makeForm("Presidential Pardon", "Booka");
	rrf2->beSigned(tmp);
	rrf2->execute(tmp);

	Form * rrf3 = SuperPuperIntern.makeForm("Shrubbery Creation", "Yeyeyeye");
	rrf3->beSigned(tmp);
	rrf3->execute(tmp);

	Form * rrf4 = CoolIntern.makeForm("Multiplying by two", "Greatest");
	rrf4->beSigned(tmp);
	return (0);
}
