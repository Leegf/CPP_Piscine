/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Intern.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 19:40:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 20:16:40 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D05_INTERN_H
#define D05_INTERN_H

#include <iostream>
#include "RobotomyRequestForm.h"
#include "PresidentialPardonForm.h"
#include "ShrubberyCreationForm.h"

class Intern {
public:
	Intern(void);
	Intern(Intern const &src);
	~Intern(void);
	Intern &operator=(Intern const &rhs);
	std::string getFormName(void) const;
	std::string getTargetName(void) const;
	class WrongRequestException : public std::exception {
	public:
		virtual const char* what() const throw();
	};
	Form * makeForm(std::string formName, std::string targetName);

private:
	std::string _formName;
	std::string _targetName;
};

#endif //D05_INTERN_H
