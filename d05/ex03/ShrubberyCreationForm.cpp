/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ShrubberyCreationForm.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 17:07:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 19:13:10 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ShrubberyCreationForm.h"
#include <fstream>

ShrubberyCreationForm::ShrubberyCreationForm(void) {
	return;
}

ShrubberyCreationForm::ShrubberyCreationForm(std::string target) {
	this->setTarget(target);
}

ShrubberyCreationForm::~ShrubberyCreationForm(void) {
	return;
}

ShrubberyCreationForm::ShrubberyCreationForm(ShrubberyCreationForm const &src) {
	*this = src;
	return;
}

ShrubberyCreationForm &ShrubberyCreationForm::operator=(ShrubberyCreationForm const &rhs) {
	if (this != &rhs) {
		this->setTarget(rhs.getTarget());
	}
	return *this;
}

void ShrubberyCreationForm::action() const {
	std::ofstream ofs;
	ofs.open(this->getTarget() + "_shrubbery");
	if (!ofs.is_open()) {
		std::cerr << "Can't write info to that file."
				  << std::endl;
		exit(1);
	}
	ofs<<"    # #### ####\n"
			"        ### \\/#|### |/####\n"
			"       ##\\/#/ \\||/##/_/##/_#\n"
			"     ###  \\/###|/ \\/ # ###\n"
			"   ##_\\_#\\_\\## | #/###_/_####\n"
			"  ## #### # \\ #| /  #### ##/##\n"
			"   __#_--###`  |{,###---###-~\n"
			"             \\ }{\n"
			"              }}{\n"
			"              }}{\n"
			"              {{}\n"
			"        , -=-~{ .-^- _\n"
			"              `}\n"
			"               {"<<std::endl;
	ofs<<"    # #### ####\n"
			"        ### \\/#|### |/####\n"
			"       ##\\/#/ \\||/##/_/##/_#\n"
			"     ###  \\/###|/ \\/ # ###\n"
			"   ##_\\_#\\_\\## | #/###_/_####\n"
			"  ## #### # \\ #| /  #### ##/##\n"
			"   __#_--###`  |{,###---###-~\n"
			"             \\ }{\n"
			"              }}{\n"
			"              }}{\n"
			"              {{}\n"
			"        , -=-~{ .-^- _\n"
			"              `}\n"
			"               {"<<std::endl;
	ofs<<"    # #### ####\n"
			"        ### \\/#|### |/####\n"
			"       ##\\/#/ \\||/##/_/##/_#\n"
			"     ###  \\/###|/ \\/ # ###\n"
			"   ##_\\_#\\_\\## | #/###_/_####\n"
			"  ## #### # \\ #| /  #### ##/##\n"
			"   __#_--###`  |{,###---###-~\n"
			"             \\ }{\n"
			"              }}{\n"
			"              }}{\n"
			"              {{}\n"
			"        , -=-~{ .-^- _\n"
			"              `}\n"
			"               {"<<std::endl;
	ofs.close();
}

std::string ShrubberyCreationForm::getName() const {
	return "Shrubbery Creation";
}

int ShrubberyCreationForm::getGradToEx() const {
	return 137;
}

int ShrubberyCreationForm::getGradToSign() const {
	return 145;
}
