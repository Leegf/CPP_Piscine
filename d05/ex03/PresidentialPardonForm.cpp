/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.cpp                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 19:25:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 19:25:22 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "PresidentialPardonForm.h"

PresidentialPardonForm::PresidentialPardonForm(void) {
	return;
}

PresidentialPardonForm::PresidentialPardonForm(std::string target) {
	this->setTarget(target);
}

PresidentialPardonForm::~PresidentialPardonForm(void) {
	return;
}

PresidentialPardonForm::PresidentialPardonForm(
		PresidentialPardonForm const &src) {
	*this = src;
	return;
}

PresidentialPardonForm &
PresidentialPardonForm::operator=(PresidentialPardonForm const &rhs) {
	if (this != &rhs)
		this->setTarget(rhs.getTarget());
	return *this;
}

std::ostream &operator<<(std::ostream &o, PresidentialPardonForm const &i) {
	o << i.getTarget();

	return o;
}

void PresidentialPardonForm::action() const {
	std::cout<<"<"<<this->getTarget()<<"> has been pardoned by Zaphod Beeblebrox."<<std::endl;
}

//getters:
std::string PresidentialPardonForm::getName() const {
	return "Presidentail Pardon";
}

int PresidentialPardonForm::getGradToEx() const {
	return 5;
}

int PresidentialPardonForm::getGradToSign() const {
	return 25;
}
