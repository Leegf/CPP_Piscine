/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   PresidentialPardonForm.h                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 19:25:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 19:27:01 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D05_PRESIDENTIALPARDONFORM_H
#define D05_PRESIDENTIALPARDONFORM_H
#include <iostream>
#include "Form.h"

class PresidentialPardonForm : public Form {
public:
	PresidentialPardonForm(void);
	PresidentialPardonForm(std::string target);
	PresidentialPardonForm(PresidentialPardonForm const &src);
	~PresidentialPardonForm(void);
	PresidentialPardonForm &operator=(PresidentialPardonForm const &rhs);
	std::string getName(void) const;
	int getGradToSign(void) const;
	int getGradToEx(void) const;
	void action(void) const;

private:

};

std::ostream &operator<<(std::ostream &o, PresidentialPardonForm const &i);

#endif //D05_PRESIDENTIALPARDONFORM_H
