/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 12:59:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 18:12:06 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D05_FORM_H
#define D05_FORM_H
#include <iostream>
class Bureaucrat;

class Form {
public:
	Form(void);
	Form(std::string formName, int gradeToSign, int gradeToEx);
	Form(Form const &src);
	virtual ~Form(void);
	Form &operator=(Form const &rhs);
	virtual std::string getName(void) const;
	virtual int getGradToSign(void) const;
	virtual int getGradToEx(void) const;
	virtual void action(void) const = 0;
	bool signedFlag;
	virtual bool getSignedFlag(void) const;
	void setTarget(std::string tg);
	std::string getTarget() const;
	void setSignedFlag(bool par);
	bool execute(Bureaucrat const & executor) const;

	bool beSigned(const Bureaucrat & bur);
	class GradeTooHighException : public std::exception {
	public:
		virtual const char* what() const throw();
	};
	class GradeTooLowException : public std::exception {
	public:
		virtual const char* what() const throw();
	};
	class FormNotSignedException : public std::exception {
	public:
		virtual const char * what() const throw();
	};

private:
	const std::string _name;
	const int _reqGradToSign;
	const int _regGradToEx;
	std::string _target;
};

std::ostream &operator<<(std::ostream &o, Form const &i);

#endif //D05_FORM_H
