/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 12:59:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 16:34:36 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D05_FORM_H
#define D05_FORM_H
#include <iostream>
class Bureaucrat;

class Form {
public:
	Form(void);
	Form(std::string formName, int gradeToSign, int gradeToEx);
	Form(Form const &src);
	~Form(void);
	Form &operator=(Form const &rhs);
	std::string getName(void) const;
	int getGradToSign(void) const;
	int getGradToEx(void) const;
	bool signedFlag;
	bool getSignedFlag(void) const;

	bool beSigned(const Bureaucrat & bur);
	class GradeTooHighException : public std::exception {
	public:
		virtual const char* what() const throw();
	};
	class GradeTooLowException : public std::exception {
	public:
		virtual const char* what() const throw();
	};

private:
	const std::string _name;
	const int _reqGradToSign;
	const int _regGradToEx;
};

std::ostream &operator<<(std::ostream &o, Form const &i);

#endif //D05_FORM_H
