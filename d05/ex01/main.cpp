/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 10:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 18:16:57 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Bureaucrat.h"
#include "Form.h"

int main(void){
//	Form tmp("ooo", 0, 130);
	Bureaucrat sir("Bob", 41);
	Form fm("spravka", 40, 41);
	fm.beSigned(sir);
	std::cout<<fm;
	sir.signForm(fm);
	return (0);
}
