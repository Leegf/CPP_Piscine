/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Bureaucrat.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 10:39:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 16:40:48 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D05_BUREAUCRAT_H
#define D05_BUREAUCRAT_H
#include <iostream>

class Form;

class Bureaucrat {
public:
	Bureaucrat(void);
	Bureaucrat(const std::string nameOfB, int grade);
	Bureaucrat(Bureaucrat const &src);
	~Bureaucrat(void);
	Bureaucrat &operator=(Bureaucrat const &rhs);
 	std::string getName(void) const;
	 int getGrade(void) const;
	void incrementGrade(void);
	void decrementGrade(void);
	void setGrade(int gr);
	class GradeTooHighException : public std::exception {
	public:
		virtual const char* what() const throw();
	};
	class GradeTooLowException : public std::exception {
	public:
		virtual const char* what() const throw();
	};

	void signForm(Form & par);
private:
	const std::string _name;
	int _grade;
};

std::ostream & operator<<(std::ostream & o, Bureaucrat const & i);

#endif //D05_BUREAUCRAT_H
