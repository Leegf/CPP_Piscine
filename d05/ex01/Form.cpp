/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Form.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/26 12:59:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/26 16:38:54 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Form.h"
#include "Bureaucrat.h"

Form::Form(void) : signedFlag(false), _name("28y"), _reqGradToSign(1), _regGradToEx(1) {
	return;
}

Form::Form(std::string formName, int gradeToSign, int gradeToEx) : signedFlag(false), _name(formName), _reqGradToSign(gradeToSign),  _regGradToEx(gradeToEx) {
	try {
		if (gradeToSign > 150 || gradeToEx > 150)
			throw GradeTooHighException();
		else if (gradeToSign < 1 || gradeToEx < 1)
			throw GradeTooLowException();
	}
	catch(GradeTooHighException & e) {
		std::cout<<"Exception occurred: "<<e.what()<<std::endl;
		exit(1);
	}
	catch(GradeTooLowException & e) {
		std::cout<<"Exception occurred: "<<e.what()<<std::endl;
		exit(1);
	}
	catch(std::exception & e) {
		std::cout<<"Standard exception caught."<<std::endl;
	}
}

Form::~Form(void) {
	return;
}

Form::Form(Form const &src) : _reqGradToSign(150), _regGradToEx(150) {
	*this = src;
	return;
}

Form &Form::operator=(Form const &rhs) {
	if (this != &rhs)
		this->signedFlag = rhs.getSignedFlag();
	return *this;
}

bool Form::beSigned(const Bureaucrat & bur) {
	try {
		if (bur.getGrade() > this->getGradToSign()) {
			throw GradeTooLowException();
		}
			else {
				this->signedFlag = true;
			return true;
			}
		}
	catch (GradeTooLowException & e) {
		std::cout<<"Form can't be signed, exception occurred: "<<e.what()<<std::endl;
		return false;
	}
	catch (std::exception & e) {
		std::cout<<"Standard exception occurred."<<std::endl;
		return false;
	}
}

std::ostream &operator<<(std::ostream &o, Form const &i) {
	o <<"<"<<i.getName()<<"> requires <"<<i.getGradToSign()<<"> to sign and <"<<i.getGradToEx()<<"> to execute. It is "<<(i.signedFlag ? "signed." : "unsigned.")<<std::endl;

	return o;
}

//getters:

bool Form::getSignedFlag() const {
	return this->signedFlag;
}

std::string Form::getName() const {
	return this->_name;
}

int Form::getGradToEx() const {
	return this->_regGradToEx;
}

int Form::getGradToSign() const {
	return this->_reqGradToSign;
}
//exceptions

const char* Form::GradeTooLowException::what() const throw() {
	return ("Grade can't be that low.");
}

const char* Form::GradeTooHighException::what() const throw() {
	return ("Grade can't be that high.");
}