# default type
```
#include <iostream>
#include <iomanip>

template<typename T = float> //we tell what to choose for its type by default.

class Vertex {

public:
Vertex(T const & x, T const & y, T const & z) : _x(x), _y(y), _z(z) { }
~Vertex( void) { }

T const & getX(void) const {return this->_x;}
T const & getY(void) const {return this->_y;}
T const & getZ(void) const {return this->_z;}

//Etc...
private:

T const _x;
T const_y;
T const_z;

Vertex(void); //impossible to construct it without passing it parameters.

};


template<typename T>
std::ostream & operator<<(std::ostream & o, Vertex<T>const & v) {
std::cout.precision(1);
o << setiosFlags(std::ios::fixed);
o << "Vertex( ";
o<<get...
o<< " )";
return o;
}

main(void) {
Vertex<int> v1(12,23,34);
Vertex<> v2(12, 23, 34); //will be using default type.

std::cout<<v1<<std::endl;
std::cout<<v2<<std::endl;

return (0);
}
```
