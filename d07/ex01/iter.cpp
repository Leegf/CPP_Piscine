/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 13:09:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/28 14:36:10 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template<typename T>

void iter(T * arr, int len, T (*func)(T&)){
	for (int i = 0; i < len; i++) {
		func(arr[i]);
	}
}

template <typename T>
T  someFunc(T & par)
{
	par++;
		std::cout<<par<<std::endl;
	return (par);
}

#define LEN 10
int		main(void){
	srand(clock());
	int * arr = new int(LEN);
//	char * arr = new char(LEN);
	for (int i = 0; i < LEN; i++) {
		arr[i] = rand() % 100;
		std::cout<<"arr["<<i<<"] = "<<arr[i];
		if (i < LEN - 1)
			std::cout<<", ";
	}
	std::cout<<std::endl;
	iter(arr, LEN, &someFunc);

	return (0);
}