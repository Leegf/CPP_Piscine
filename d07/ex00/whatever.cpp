/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whatever.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 12:37:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/28 13:00:05 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>

template<typename T>

void swap(T & par1, T & par2) {
	T tmp = par1;
	par1 = par2;
	par2 = tmp;
}

template<typename T>
T const & min(T const & x, T const & y) {
	return (y <= x) ? y : x;
}

template<typename T>
T const & max(T const & x, T const & y) {
	return (y >= x) ? y : x;
}

/*int 	main(void) {
	srand(clock());

*//*	int x = rand() % 100;
	int y = rand() % 320;*//*
	float x = rand() % 300 + 0.571;
	float y = rand() % 350 + 0.571;
	std::cout<<"Initial values : "<<x<<" "<<y<<std::endl;
	swap(x, y);
	std::cout<<"Swapped values : "<<x<<" "<<y<<std::endl;
	std::cout<<"The smallest: "<<min(x, y)<<std::endl;
	std::cout<<"The biggest: "<<max(x, y)<<std::endl;
	return (0);
}*/

int main( void ) { int a = 2;
	int b = 3;
	::swap( a, b );
	std::cout << "a = " << a << ", b = " << b << std::endl;
	std::cout << "min( a, b ) = " << ::min( a, b ) << std::endl;
	std::cout << "max( a, b ) = " << ::max( a, b ) << std::endl;
	std::string c = "chaine1";
	std::string d = "chaine2";
	::swap(c, d);
	std::cout << "c = " << c << ", d = " << d << std::endl;
	std::cout << "min( c, d ) = " << ::min( c, d ) << std::endl;
	std::cout << "max( c, d ) = " << ::max( c, d ) << std::endl;
	return 0; }
