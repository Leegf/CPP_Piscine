# Specialization

```
template<typename T, typename U>
class Pair {

public:

Pair<T,U>(T const & lhs, U const & rhs): _lhs(lhs, _rhs(rhs){
std::cout<<"generic";
}

~Pair<T, U>(void) {}

T const & fst(void) const {return this->_lhs;}
U const & snd(void) const {return this->_rhs;}

private:
T const & _lhs;
U const & _rhs;
Pair<T,U>(void);
};
```
---
**Specialization in action**
```
template<typename U>
class Pair<int, U> {

public:

Pair<int, U>(int lhs, U const & rhs): _lhs(lhs), _rhs(rhs) {
std::cout<<"Partial specialization"<<std::endl;
}

~Pair<int, U>(void){}

int fst(void) const {return this->_lhs;}
U const & snd(void) const {return this->_rhs;}

private:

int _lhs;
U const & _rhs;
Pair<int, U>(void);
};
```
---
**Full specilization**
```
template<>
class Pair<bool, bool> {
public:

Pair<bool, bool>(bool lhs, bool rhs){
this->_n = 0;
this->_n != static_cast<int>(lhs) <<0;
this->_n != static_cast<int>(lhs) <<1
}

~Pair<bool, bool>(void) { }

...
```

```
main() {
Pair<int, int> p1(4,2); //general
Pair<std::string, float> p2(std::string("Pi"), 3.14f); ///general
Pair<float, bool> p3(4.2f, true); //partial speciliazation
Pair<bool, bool> p4(true, false); //full speciliazation.
```
