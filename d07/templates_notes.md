# Templates

**Templates, max**

```
#include <iostream>

template< typename T > //announcing template in which we delclare a type.

T /*const &*/ max(T const & x, T const & y) { //references might be used here as well.
return ( x>y ? x : y);
}

int main(void){
int a = 21;
int b = 42;

std::cout<<max<int>(a,b)<<std::endl; //explicit instanciation
std::cout<<max(a,b)<<std::endl; //implicit instanciation
}
```
**Data structures**

```
template <typename T>
to introduce a second name parameter
template <typename T, typename U>

class List {

public:

	List<T>(T const & content) {
	// Etc...
	}

	List<T>(List<T> const & list {
	//etc..
	}
	
	~List<T>(void) {
	//etc..
	}

	//etc...
private:
T * _content; //we could remove star.
List<T> * _next;
};

int main(void) {
List<int> a (42);
//List<int, float> a (42); ,for two types.
List <float> b(3.14f);
List<List <int > > c(a); // list of list of integers.

return (0);
```
