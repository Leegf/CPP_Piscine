/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 14:38:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/28 20:25:37 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include "Array.h"

int main(void) {

	Array<int> boo(10);
	Array <int> boo2;

	try {

		for (int i = 0; i < 10; i++) {
			boo[i] = i;
			std::cout << boo[i] << std::endl;
		}
	}
	catch (std::out_of_range &e) {
		std::cout<<"Exception caught: "<<e.what()<<std::endl;
		return (1);
	}
	boo2 = boo;
	std::cout<<std::endl;
	try {

		for (int i = 0; i < 10; i++) {
			std::cout << boo2[i] << std::endl;
		}
	}
	catch (std::out_of_range &e) {
		std::cout<<"Exception caught: "<<e.what()<<std::endl;
		return (1);
	}
	return (0);
}
