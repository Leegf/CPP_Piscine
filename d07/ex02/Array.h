/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Array.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/28 15:02:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/28 20:32:05 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef D07_ARRAY_H
#define D07_ARRAY_H

#include <cstdlib>
#include <iostream>

template <typename T>
class Array {
public:
	Array(void) : _size(0) {
		this->_ptr = nullptr;
	}
	Array(unsigned int N) : _size(N) {
		this->_ptr = new T[N]();
	}
	Array(Array<T> const &src) {
		*this = src;
	};
	~Array(void) {
		if (this->_ptr != nullptr)
			delete [] this->_ptr;
	}
	T * getPtr(void) const {
		return this->_ptr;
	}
	unsigned int size(void) const {
		return this->_size;
	}
/*	class OutOfBoundariesException : public std::exception {
	public:
		virtual const char* what() const throw() {
			return "Element you're trying to access is out of boundaries.";
		}
	};*/
	Array &operator=(Array<T> const &rhs){
			if (this->getPtr() != nullptr)
				delete [] this->_ptr;
			this->_ptr = new T[rhs.size()]();
		for (unsigned int i = 0; i < rhs.size(); i++) {
			this->_ptr[i] = rhs[i];
		}
			this->_size = rhs.size();
		return *this;
	}

	T& operator[](unsigned int i) const {
			try {
				if (i > (this->_size - 1))
					throw std::out_of_range("n");
			}
			catch(std::out_of_range & e) {
				throw std::out_of_range("It's out of boundaries.");
			}
		return this->_ptr[i];
	}
private:
	T * _ptr;
	unsigned int _size;
};

#endif //D07_ARRAY_H
