/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Account.class.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 21:34:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/20 11:28:21 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Account.class.hpp"
#include <ctime>
#include <iostream>

Account::Account(int initial_deposit) : _accountIndex(Account::_nbAccounts),
		_amount(initial_deposit),_nbDeposits(0), _nbWithdrawals(0)
{
	Account::_displayTimestamp();
	std::cout<<"index:"<<this->_accountIndex<<";amount:"<<
				this->checkAmount()<<";created"<<std::endl;
	Account::_totalAmount += this->_amount;
	Account::_nbAccounts++;
	return;
}

Account::~Account(void)
{
	Account::_displayTimestamp();
	std::cout<<"index:"<<this->_accountIndex<<";amount:"<<
			 this->checkAmount()<<";closed"<<std::endl;
	return;
}

void 	Account::makeDeposit(int deposit)
{
	int tmp;

	tmp = this->_amount;
	this->_amount += deposit;
	Account::_totalAmount += deposit;
	this->_nbDeposits++;
	Account::_totalNbDeposits += this->_nbDeposits;
	Account::_displayTimestamp();
	std::cout<<"index:"<<this->_accountIndex<<";p_amount:"<<tmp<<";deposit:"<<
   deposit<<";amount:"<<checkAmount()<<";nb_deposits:"<<this->_nbDeposits<<std::endl;
}

bool Account::makeWithdrawal(int withdrawal)
{
	Account::_displayTimestamp();
	std::cout<<"index:"<<this->_accountIndex<<";p_amount:"<<checkAmount();
	if (withdrawal > this->_amount)
	{
		std::cout<<";withdrawal:refused"<<std::endl;
		return (false);
	}
	this->_amount -= withdrawal;
	Account::_totalAmount -= withdrawal;
	this->_nbWithdrawals++;
	Account::_totalNbWithdrawals++;
	std::cout<<";withdrawal:"<<withdrawal<<";amount:"<<checkAmount()<<
	";nb_withdrawals:"<<this->_nbWithdrawals<<std::endl;
	return (true);
}

int 	Account::checkAmount(void) const
{
	return this->_amount;
}

void 	Account::displayStatus(void) const
{
	Account::_displayTimestamp();
	std::cout<<"index:"<<this->_accountIndex<<";amount:"<<
			this->checkAmount()<<";deposits:"<<this->_nbDeposits<<
			";withdrawals:"<<this->_nbWithdrawals<<std::endl;
}

void 	Account::_displayTimestamp(void)
{
	time_t here_time;
	struct tm * timeinfo;
	char buffer[20];

	bzero(buffer, 20);
	time (&here_time);
	timeinfo = localtime(&here_time);
	strftime(buffer, 20, "[%Y%m%d_%H%M%S]", timeinfo);
	std::cout << buffer << " ";
}

int Account::getNbAccounts(void) {
	return Account::_nbAccounts;
}

int Account::getTotalAmount(void) {
	return Account::_totalAmount;
}

int Account::getNbDeposits(void) {
	return Account::_totalNbDeposits;
}

int Account::getNbWithdrawals (void) {
	return Account::_totalNbWithdrawals;
}

void	Account::displayAccountsInfos(void) {
	Account::_displayTimestamp();
	std::cout<<"accounts:"<<Account::getNbAccounts()<<";total:"<<
	Account::getTotalAmount()<<";deposits:"<<Account::getNbDeposits()
	<<";withdrawals:"<<Account::getNbWithdrawals()<<std::endl;
}

int Account::_nbAccounts = 0;
int Account::_totalAmount = 0;
int Account::_totalNbDeposits = 0;
int Account::_totalNbWithdrawals = 0;
