/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.class.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 17:47:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/18 21:20:18 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.class.h"

Contact::Contact(void)
{
	this->available = false;
	return ;
}

Contact::~Contact(void)
{
	return ;
}

void Contact::_setData(std::string first_name, std::string last_name,
					  std::string nickname, std::string login,
					  std::string postal_address, std::string email_address,
					  std::string phone_number, std::string birthdate,
					  std::string meal, std::string underwear,
					  std::string secret)
{
	this->_first_name = first_name;
	this->_last_name = last_name;
	this->_nickname = nickname;
	this->_login = login;
	this->_postal_address = postal_address;
	this->_email_address = email_address;
	this->_phone_number = phone_number;
	this->_birthdate = birthdate;
	this->_meal = meal;
	this->_underwear = underwear;
	this->_secret = secret;
	std::cout<<"New contact successfully has been added." <<std::endl;
}

void	Contact::put_all_data(void) const
{
	std::cout <<"First name: " << this->_first_name << std::endl;
	std::cout <<"Last name: " << this->_last_name << std::endl;
	std::cout <<"Nickname: " << this->_nickname << std::endl;
	std::cout <<"Login: " << this->_login << std::endl;
	std::cout <<"Postal address: " << this->_postal_address << std::endl;
	std::cout <<"Email address: " << this->_email_address << std::endl;
	std::cout <<"Phone number: " << this->_phone_number << std::endl;
	std::cout <<"Birthday date: " << this->_birthdate << std::endl;
	std::cout <<"Favorite meal: " << this->_meal << std::endl;
	std::cout <<"Color of underwear: " << this->_underwear << std::endl;
	std::cout <<"Secret: " << this->_secret << std::endl;
}

bool	Contact::promptUser(int i) {
	std::string arr[11];
	int j;

	if (i > 7)
		return(false);
	j = 0;
	while (arr[j].compare("") == 0)
	{
		std::cout << "Enter first name: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter last name: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter nickname: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter login: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter postal address: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter email address: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter phone number: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter birthday date: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter your favorite meal: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter your the color of your underwear: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	j++;
	while (arr[j].compare("") == 0) {
		std::cout << "Enter your darkest secret: ";
		std::getline(std::cin, arr[j]);
		if (std::cin.eof())
			exit(0);
	}
	this->_WriteToData(arr);
	this->available = true;
	return (true);
}

void	Contact::_WriteToData(std::string *arr) {
	this->_setData(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6],
	arr[7], arr[8], arr[9], arr[10]);
}

std::string	Contact::getFirstName(void) const {
	return this->_first_name;
}

std::string	Contact::getLastName(void) const {
	return this->_last_name;
}

std::string	Contact::getNickname(void) const {
	return this->_nickname;
}
