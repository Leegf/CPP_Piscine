/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 17:28:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/19 16:21:46 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Contact.class.h"
#include <iomanip>
#include <stdlib.h>

void	show_available(Contact contacts[8])
{
	std::string tmp;

	for (int i = 0; i < 8; i++)
	{
		if (contacts[i].available)
		{
			std::cout << std::setw(10) << i << '|';
			if (contacts[i].getFirstName().length() > 10)
				tmp = contacts[i].getFirstName().substr(0, 9) + ".";
			else
				tmp = contacts[i].getFirstName();
			std::cout << std::setw(10) << tmp << '|';
			if (contacts[i].getLastName().length() > 10)
				tmp = contacts[i].getLastName().substr(0, 9) + ".";
			else
				tmp = contacts[i].getLastName();
			std::cout << std::setw(10) << tmp << '|';
			if (contacts[i].getNickname().length() > 10)
				tmp = contacts[i].getNickname().substr(0, 9) + ".";
			else
				tmp = contacts[i].getNickname();
			std::cout << std::setw(10) << tmp << '|'<<std::endl;
		}
	}
}

void	show_contact(Contact contacts[8])
{
	std::string tmp;

	std::cout << "Enter index of the contact you want to see: ";
	std::getline(std::cin, tmp);
	if (std::cin.eof())
		exit(0);
	if (atoi(tmp.c_str()) < 8 && atoi(tmp.c_str()) >= 0 && tmp.length() == 1 && contacts[atoi(tmp.c_str())].available)
		contacts[atoi(tmp.c_str())].put_all_data();
	else
		std::cout << "Put something relevant here." <<std::endl;
}

int main(void)
{
	std::string buff;
	int i;
	int flag;

	i = 0;
	flag = 0;
	Contact contacts[8];
	while (std::getline(std::cin, buff))
	{
		if (buff.compare("EXIT") == 0)
			exit(0);
		else if (buff.compare("ADD") == 0)
		{
			flag++;
			if(!contacts[i].promptUser(i))
				std::cout << "You can't now use ADD command. It has 8 already stored contacts." <<std::endl;
			i++;
		}
		else if (buff.compare("SEARCH") == 0)
		{
			if (!flag)
			{
				std::cout << "Sorry, but you haven't added anything just yet."<<std::endl;
				continue ;
			}
			show_available(contacts);
			show_contact(contacts);
		}
		else
			std::cout <<"Available commands: ADD, SEARCH, EXIT" <<std::endl;
	}
	return (0);
}
