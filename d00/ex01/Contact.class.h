/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Contact.class.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 17:47:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/06/18 20:36:39 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONTACT_CLASS_H
#define CONTACT_CLASS_H
#include <iostream>

class Contact {

public:
	Contact(void);
	~Contact(void);
	bool promptUser(int i);
	bool available;
	std::string getFirstName(void) const;
	std::string getLastName(void) const;
	std::string getNickname(void) const;
	void put_all_data(void) const;

private:
	std::string _first_name;
	std::string _last_name;
	std::string _nickname;
	std::string _login;
	std::string _postal_address;
	std::string _email_address;
	std::string _phone_number;
	std::string _birthdate;
	std::string _meal;
	std::string _underwear;
	std::string _secret;
	void _setData(std::string first_name, std::string last_name,
				 std::string nickname, std::string login, std::string postal_address,
				 std::string email_address, std::string phone_number, std::string birthdate,
				 std::string meal, std::string underwear, std::string secret);
	void _WriteToData(std::string arr[11]);
};

#endif
