cmake_minimum_required(VERSION 3.9)
project(d00)

set(CMAKE_CXX_STANDARD 98)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Werror")

add_executable(megaphone ex00/megaphone.cpp)
add_executable(phone ex01/main.cpp ex01/Contact.class.cpp ex01/Contact.class.h)
add_executable(rere ex02/tests.cpp ex02/Account.class.cpp ex02/Account.class.h)
